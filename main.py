# https://stackoverflow.com/a/18463100
from discord import File, Client
from discord.ext import commands
from asyncio import sleep, TimeoutError
from re import split
from commands.pricing_data.pricing import create_invalid_embed
from io import BytesIO

def get_token():
    from json import load
    token = load(open("token.json", "r"))
    return token["bot_token"]

def get_dune_headers():
    return {
        "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language":"en-US,en;q=0.5",
        "Cache-Control": "no-cache",
        "Content-Length" : "415",
        "content-type": "application/json",
        "DNT": "1",
        "Host": "core-hsr.duneanalytics.com",
        "Origin": "https://dune.xyz/",
        "Referer": "https://dune.xyz",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "no-cors",
        "Sec-Fetch-Site": "cross-site",
        "TE":"Trailers",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0",
    }

class MyClient(Client):

    def __init__(self):
        super().__init__()
        self.symbol_lists = None
        self.funding_rate_lists = None
        self.dune_url = "https://core-hsr.duneanalytics.com/v1/graphql"
        self.market_bias_data = None
        self.liquidated_data = None
        self.funding_data = None
        self.headers = get_dune_headers()
        self.aggregated_funding = None
        self.perp_holdings = None
        self.sperp_holdings = None
        self.staked_immediate_dates_data = None
        self.staked_vested_dates_data = None
        self.staked_immediate_data = None
        self.staked_vested_data = None
        self.leaderboard_data = None

    async def resync_database(self):
        from commands.perpetual.perp.perp_data import get_amm_funding, \
            get_market_bias_data, get_liquidations_data, \
            get_liquidations_by_symbol_data, retrieve_funding, \
            get_aggregated_funding, get_perp_holdings, get_staked_perp_holdings, \
            retrieve_staked_url, retrieve_staked_vesting_url, \
            retrieve_staking_data, retrieve_leaderboard_data
        from commands.perpetual.perp.perp_helper import format_liquidations, \
            format_liquidations_by_days, get_formatted_perp_staked_urls
        from time import time

        start_time = time()
        symbols = None
        funding_rates = None
        print("START GETTING DATA")

        self.staked_immediate_dates_data = await retrieve_staked_url()
        self.staked_vested_dates_data = await retrieve_staked_vesting_url()
        formatted_i_staked_urls = get_formatted_perp_staked_urls(self.staked_immediate_dates_data)
        formatted_v_staked_urls = get_formatted_perp_staked_urls(self.staked_vested_dates_data)
        self.staked_immediate_data = retrieve_staking_data(formatted_i_staked_urls, None, False)
        self.staked_vested_data = retrieve_staking_data(formatted_v_staked_urls, self.staked_vested_dates_data, True)
        self.market_bias_data = get_market_bias_data(self.headers, self.dune_url)
        self.liquidated_data = get_liquidations_data(self.headers, self.dune_url)[:7]
        self.liquidated_data = format_liquidations(self.liquidated_data)
        self.liquidated_symbol_data = get_liquidations_by_symbol_data(self.headers, self.dune_url)
        self.liquidated_symbol_data = format_liquidations_by_days(self.liquidated_symbol_data, 7)
        self.aggregated_funding = get_aggregated_funding(self.headers, self.dune_url)
        self.funding_data = await retrieve_funding()
        self.funding_data = self.funding_data["query_result"]["data"]["rows"]
        self.perp_holdings = get_perp_holdings(self.headers, self.dune_url)
        self.sperp_holdings = get_staked_perp_holdings(self.headers, self.dune_url)
        # May or may not timeout, depending on whether the subgraph is happy.
        self.leaderboard_data = retrieve_leaderboard_data()
        print("FINISHED GETTING DATA")
        while True:

            print("START SYNC FUNDING")
            try:
                symbols, funding_rates =  get_amm_funding(None)
            except Exception as e:
                print("ERROR")
                print(e)
                continue
            if symbols and funding_rates:
                self.symbol_lists = symbols
                self.funding_rate_lists = funding_rates
                print("FINISHED SYNC FUNDING")
            if (time() - start_time) >= 86400:
                start_time = time()
                # await add_new_coins()
                # await add_top_coins()
                self.staked_immediate_dates_data = await retrieve_staked_url()
                self.staked_vested_dates_data = await retrieve_staked_vesting_url()
                formatted_i_staked_urls = get_formatted_perp_staked_urls(self.staked_immediate_dates_data)
                formatted_v_staked_urls = get_formatted_perp_staked_urls(self.staked_vested_dates_data)
                self.staked_immediate_data = retrieve_staking_data(formatted_i_staked_urls, None, False)
                self.market_bias_data = get_market_bias_data(self.headers, self.dune_url)
                self.liquidated_data = format_liquidations(get_liquidations_data(self.headers, self.dune_url)[:7])
                self.liquidated_symbol_data = format_liquidations_by_days(get_liquidations_by_symbol_data(self.headers, self.dune_url), 7)
                self.aggregated_funding = get_aggregated_funding(self.headers, self.dune_url)
                self.funding_data = await retrieve_funding()
                self.funding_data = self.funding_data["query_result"]["data"]["rows"]
                self.perp_holdings = get_perp_holdings(self.headers, self.dune_url)
                self.sperp_holdings = get_staked_perp_holdings(self.headers, self.dune_url)
                self.leaderboard_data = await retrieve_leaderboard_data()
            await sleep(1000)

    async def on_ready(self):
        from discord import Activity, ActivityType
        await self.change_presence(activity = Activity(type=ActivityType.watching, name="Trading"))
        print('Logged on as {0}!'.format(self.user))
        await self.resync_database()

    async def on_message(self, message):

        if message.author.id == 249079413696757760:
            if message.content == "intro":
                from commands.help_info.helper_function import create_help_info
                embed = create_help_info()
                await message.channel.send(embed = embed)
                await message.delete()

        if message.content.lower().startswith(("perp " , "perpx ", "perpetualx ")):
            arguments = " ".join(message.content.split()[1:]).lower()
            if arguments in ["social", "socials"]:
                from commands.socials.social_links import create_social_embed
                social_embed = create_social_embed()
                await message.channel.send(embed = social_embed)

            elif arguments in ["mkt", "markets", "exchange", "ex", "exchanges"]:
                from commands.markets.market_links import create_markets_embed
                market_collection = create_markets_embed()
                await self.process_cycle_embeds(market_collection, message)

            elif arguments in ["oi", "open interest", "interest"]:
                from commands.perpetual.perp.perp_functions import get_perp_open_interest
                open_interest_collection = await get_perp_open_interest()
                await self.process_cycle_embeds(open_interest_collection, message)

            if arguments in ["txs", "transactions"]:
                from commands.perpetual.perp.perp_functions import get_last_transactions
                tx_collection = get_last_transactions()
                await self.process_cycle_embeds(tx_collection, message)

            elif arguments.startswith(("liq", "liquid", "liquidity", "vol", "volume", "volumes")):
                # Plots Liquidity Charts
                from commands.perpetual.charts.chart_functions import get_liquidity_chart, get_volume_chart

                async with message.channel.typing():

                    is_liquidity = 1 if arguments.startswith(("liq ", "liquid", "liquidity")) else 0

                    arguments = " ".join(arguments.split()[1:]).lower()
                    if not arguments:

                        # Default is 7 days
                        arguments = 7

                    if isinstance(arguments, str) and not arguments.isnumeric():
                        from commands.perpetual.charts.chart_helper import check_days
                        is_valid_day = check_days(arguments)

                        # Couldn't find valid day
                        if not is_valid_day:
                            # error_message = "The value specified is not numeric. " + \
                            # "Please specify a valid day numerically.\n"
                            # await self.send_invalid_embed(error_message, message)
                            return
                        arguments = is_valid_day
                    arguments = int(arguments)

                    # Argument must be 3 days (to show a chart that is viewable)
                    if arguments < 3:
                        # error_message = f"The value specified is less than 3 days. " + \
                        # " Please consider specifying a valid number of days greater than this.\n"
                        # await self.send_invalid_embed(error_message, message)
                        return

                    if is_liquidity:
                        img_bytes = await get_liquidity_chart(arguments)
                    else:
                        img_bytes = await get_volume_chart(arguments)
                    await message.channel.send(file = File(fp = img_bytes,
                    filename= "chart.png"))

                    collect()

            elif arguments == "trades":
                from commands.perpetual.perp.perp_functions import get_perp_trades
                trade_collection = await get_perp_trades()
                await self.process_cycle_embeds(trade_collection, message)

            elif arguments.startswith("help"):
                from commands.help_info.helper_function import create_main_help_embed, \
                    create_bias_embed, create_liquidations_embed, create_volume_embed, \
                    create_profile_embed, create_liquidity_embed, create_tv_chart_embed, \
                    create_staked_embed

                arguments = " ".join(message.content.split()[2:]).lower()
                if arguments == "staking":
                    embed = create_staked_embed()
                    await message.channel.send(embed = embed)
                elif arguments == "liquidity":
                    embed = create_liquidity_embed()
                    await message.channel.send(embed = embed)
                elif arguments == "c":
                    embed = create_tv_chart_embed()
                    await message.channel.send(embed = embed)
                elif arguments == "volume":
                    embed = create_volume_embed()
                    await message.channel.send(embed = embed)
                elif arguments == "bias":
                    embed = create_bias_embed()
                    await message.channel.send(embed = embed)
                elif arguments == "profile":
                    embed = create_profile_embed()
                    await message.channel.send(embed = embed)
                elif arguments == "fc":
                    embed = create_profile_embed()
                    await message.channel.send(embed = embed)
                elif arguments == "liq":
                    embed = create_liquidations_embed()
                    await message.channel.send(embed = embed)
                # elif arguments == "chart":
                #     embed = create_chart_embed()
                #     await message.channel.send(embed = embed)
                elif not arguments:
                    embeds = create_main_help_embed()
                    await self.process_cycle_embeds(embeds, message)

        if message.content.lower().startswith(("cx ", "c ")):
            # Linux prerequisite.
            # https://stackoverflow.com/a/63751165
            # https://techoverflow.net/2020/09/29/how-to-fix-pyppeteer-pyppeteer-errors-browsererror-browser-closed-unexpectedly/
            from commands.tradingview.tradingview_charts import \
                create_invalid_chart_embed, get_chart, check_indicators, \
                check_timeframes

            queries = split(f", c | c |, |, cx | cx ", message.content.split(" ", 1)[1])
            for query in queries:
                async with message.channel.typing():
                    arguments = query.split()
                    symbol = arguments[0]
                    if symbol.lower() == "ohm":
                        symbol += "dai"
                    arguments = arguments[1:]
                    is_wide = "wide" in arguments
                    if symbol == "ohm":
                        symbol = "ohmdai"
                    if symbol == "shorts":
                        symbol = "btcusdshorts"
                    if symbol == "longs":
                        symbol = "btcusdlongs"
                    if symbol == "perp":
                        symbol = "perpusd"
                    if is_wide:
                        arguments.remove("wide")
                        arguments = arguments if arguments else None

                    timeframes = ["1h"]
                    indicator_list = None

                    if arguments:
                        arguments = set(arguments)
                        indicator_list, arguments = check_indicators(arguments)
                        if arguments:
                            timeframes = check_timeframes(arguments)
                            if not isinstance(timeframes, list):
                                await message.channel.send(embed = timeframes)
                                continue

                    for tf in timeframes:
                        result = get_chart(symbol.upper(), tf, indicator_list)
                        if isinstance(result, str):
                            from PIL import Image
                            from pyppeteer import launch
                            from http.client import BadStatusLine
                            from psutil import Process, wait_procs
                            from gc import collect
                            logo = Image.open("logo.png")

                            # Resize logo
                            logo.thumbnail((45,45), Image.ANTIALIAS)
                            width, height = 800, 560
                            if is_wide:
                                width = 1600
                            i = 0
                            while i < 3:
                                try:
                                    args = ['--no-sandbox', '--disable-setuid-sandbox0',
                                        '--disable-dev-shm-usage']
                                    # ['--no-sandbox', '--disable-dev-shm-usage', '--disable-setuid-sandbox']
                                    if is_wide:
                                        args.append(f"--window-size={width},600")
                                        browser = await launch(headless=True,
                                        args=args, autoClose = False,
                                        defaultViewport = None)
                                    else:
                                        browser = await launch(headless = True,
                                        args = args, autoClose = False)
                                    procs = Process().children(recursive=True)
                                    try:
                                        page = await browser.newPage()
                                        if is_wide:
                                            await page.setViewport({"width": width,
                                            "height": 600})
                                        # networkidle0 domcontentloaded
                                        await page.goto(result, {"waitUntil": "networkidle0"})
                                        procs = Process().children(recursive=True)
                                        await page.waitForXPath("//script[@src='https://www.google-analytics.com/analytics.js']")
                                        await page.waitForSelector('div[data-name="legend-series-item"]', {"visible": "true"})
                                        # procs = Process().children(recursive=True)
                                        screenshot = await page.screenshot()
                                        procs = Process().children(recursive=True)
                                        image = Image.open(BytesIO(screenshot))
                                        image = image.crop((0, 0, width, height))
                                        image.paste(logo, (55, height - 90), logo)
                                        # https://stackoverflow.com/a/63210850
                                        with BytesIO() as image_binary:
                                            image.save(image_binary, "PNG")
                                            image_binary.seek(0)
                                            td_chart = await message.channel.send(file = File(fp = image_binary,
                                            filename="chart.png"))
                                            # await td_chart.add_reaction("✅")
                                            image.close()
                                            logo.close()
                                            # procs = Process().children(recursive=True)
                                            break
                                    except TimeoutError:
                                        await sleep(1)
                                        i += 1
                                    finally:
                                        await browser.close()
                                        # Zombie processes may still exist depending on OS.
                                        # _, still_alive = wait_procs(procs, timeout=2.5)
                                        # for p_ in still_alive:
                                        #     p_.terminate()
                                        # _, still_alive = wait_procs(procs, timeout=40)
                                        # for p_ in still_alive:
                                        #     p_.kill()
                                        collect()
                                except BadStatusLine:
                                    await sleep(1.15)
                                    i += 1
                                    continue
                            if i == 3:
                                result = create_invalid_chart_embed("Unfortunately I could not process the chart on time.")
                            # else:
                                # try:
                                #     reaction, _ = await self.wait_for("reaction_add",
                                #     timeout = 10.0, check = lambda _, user: user.id == message.author.id)
                                #     if str(reaction) == "✅":
                                #         await td_chart.delete()
                                # except TimeoutError:
                                #     await td_chart.clear_reactions()
                                continue
                        else:
                            await message.channel.send(embed = result)
        # Longs vs Shorts.
        if message.content.lower().startswith(("bias", "biasx")):
            from commands.perpetual.charts.chart_functions import \
                get_market_bias_collection_chart
            from commands.perpetual.charts.chart_helper import check_days
            from commands.perpetual.perp.perp_helper import lookup_symbol
            from commands.perpetual.perp.perp_functions import get_amm_bias
            msg_split = message.content.lower().split()[1:]
            arguments = " ".join(msg_split).lower()

            # If no arguments are provided create collection. Otherwise create an
            # AMM based plot.
            if not arguments:
                result = get_market_bias_collection_chart(self.market_bias_data, 1, "size")
                await message.channel.send(file = File(fp = result, filename= "chart.png"))
            else:
                # Default Preferences.
                symbol = False
                timeframe = 1
                bias_type = "size"

                # To prevent further modication.
                # If user types bias perp uni comp ..., only records perp.
                is_amm = False
                is_day = False
                is_bias_type = False

                # Identify whether a amm, day or bias_type is mentioned, if so
                # modify it.
                msg_split = msg_split[:3]
                for msg in msg_split:
                    is_valid_day = check_days(msg)
                    is_symbol = lookup_symbol(msg.upper())
                    if not is_amm and is_symbol:
                        symbol = is_symbol["id"].upper()
                        is_amm = True
                    if not is_day and is_valid_day:
                        timeframe = int(is_valid_day)
                        is_day = True
                    if not is_bias_type and msg.lower() in ["count", "size", "ratio"]:
                        bias_type = msg
                        is_bias_type = True


                # If not symbol is provided create a collection bias chart.
                # Otherwise create a AMM bias embed.
                if not symbol:
                    result = get_market_bias_collection_chart(self.market_bias_data, timeframe, bias_type)
                    await message.channel.send(file = File(fp = result, filename= "chart.png"))
                else:
                    result = get_amm_bias(self.market_bias_data, symbol, timeframe,
                    bias_type)
                    await message.channel.send(embed = result)

        if message.content.lower().startswith(("liqx" , "liq", "liquidation", "liquidations")):
            from commands.perpetual.perp.perp_functions import get_liquidations, \
                create_liquidations_embed
            arguments = " ".join(message.content.lower().split()[1:]).lower()


            # If no arguments are provided create collection. Otherwise create an
            # AMM based plot.
            if not arguments:
                result = create_liquidations_embed(self.liquidated_data)
            else:
                result = get_liquidations(self.liquidated_symbol_data, arguments)
                # Error Embed to be made (but have been suggested to remove these)
                if not result:
                    return
            await message.channel.send(embed = result)


        if message.content.lower().startswith(("p perp", "px perp")):
            from commands.pricing_data.pricing import get_pricing
            price_embed = get_pricing("perp")
            await message.channel.send(embed = price_embed)

        if message.content.lower().startswith(("mx perp", "m perp", "info perp", "infox perp")):
            from commands.pricing_data.pricing import get_info
            info_embed = get_info("perp")
            await message.channel.send(embed = info_embed)

        if message.content.lower().startswith("funding"):
            from commands.perpetual.perp.perp_functions import get_funding, create_funding_collection_embed
            from commands.pricing_data.pricing import create_invalid_embed
            from commands.perpetual.perp.perp_helper import lookup_symbol
            arguments = " ".join(message.content.split()[1:]).lower()


            # If no arguments or arguments are long or short (identifies whether
            # funding is paying for these positions). Otherwise create an AMM
            # based embed.
            if not arguments or arguments in ["long", "short"]:
                result = await create_funding_collection_embed(self.symbol_lists, self.funding_rate_lists, arguments)
            else:
                result = get_funding(self.funding_rate_lists, self.symbol_lists, arguments)

                # Error Embed to be made (but have been suggested to remove these)
                if not result:
                    return
            await message.channel.send(embed = result)

        if message.content.lower().startswith("profile"):
            from commands.perpetual.perp.perp_functions import get_profile_address
            split_message = message.content.split()[1:]

            if (split_message == "register"):
                # Will add code to register users, so their avatars can be displayed
                # in the profile embeds.
                pass
            elif (split_message == "remove"):
                # Will add code to remove users, so their avatars can be removed
                # in the profile embeds.
                pass
            else:
                split_message = " ".join(split_message).lower()
                result = await get_profile_address(split_message,
                    self.aggregated_funding, self.perp_holdings, self.sperp_holdings)

                if not result:
                    return
                await message.channel.send(embed = result)
                pass

        if message.content.lower().startswith("staking "):
            from commands.perpetual.perp.perp_functions import get_staking_address, \
                get_staking_vesting_dates
            split_message = message.content.split()[1:]

            # Staking vesting for a particular address. Otherwise retrieve staking
            # Details for a an address.
            if split_message[0].lower() == "vesting":
                split_message = " ".join(split_message[1:])
                result = get_staking_vesting_dates(split_message, self.staked_vested_data,
                self.staked_vested_dates_data)
            else:
                split_message = " ".join(split_message)
                result = get_staking_address(split_message, self.staked_vested_data,
                self.staked_vested_dates_data, self.staked_immediate_data,
                self.sperp_holdings)
            if isinstance(result, bool):
                return
            else:
                await message.channel.send(embed = result)

        if message.content.lower().startswith(("leaderboard", "lb", "board")):
            from commands.perpetual.perp.perp_functions import create_leaderboard_embed
            await message.channel.send(embed = create_leaderboard_embed(self.leaderboard_data))

        if message.content.lower().startswith(("fundx ", "fund ", "fc ")):
            from commands.perpetual.charts.chart_functions import get_funding_chart
            from commands.perpetual.charts.chart_helper import check_days
            from commands.perpetual.perp.perp_helper import lookup_symbol

            split_message = message.content.split()[1:3]

            # DEFAULT
            time_period = 7
            amm = lookup_symbol("PERP")

            is_amm = False
            is_time_period = False

            # RECEIVED VALUES (CAN LOOP FOR A LONG TIME)
            for msg in split_message:
                is_valid_day = check_days(msg)

                arguments = lookup_symbol(msg.upper())
                if not is_amm and arguments:
                    is_amm = True
                    amm = arguments
                if not is_time_period and is_valid_day:
                    is_time_period = True
                    time_period = is_valid_day

            img_bytes = get_funding_chart(self.funding_data, amm, time_period)
            await message.channel.send(file = File(fp = img_bytes, filename= "chart.png"))

    async def process_cycle_embeds(self, embeds, message):
        # Will need to fix this, normal reactions added are not allowed in dms.
        # Unless made as raw_reaction_add, which has other things involved.
        if self.user == message.author:
            await message.channel.send("This command can only be done on the Perp Server!")
            return
        msg = await message.channel.send(embed = embeds[0])
        await msg.add_reaction("⏮")
        await msg.add_reaction("◀")
        await msg.add_reaction("▶")
        await msg.add_reaction("⏭")
        await msg.add_reaction("✅")
        msg_author =message.author
        msg_author_id = message.author.id
        reaction = None
        i = 0
        while True:
            if isinstance(reaction, str):
                if reaction == "⏮":
                    if i > 0:
                        i = 0
                        await msg.edit(embed = embeds[i])
                elif reaction == "◀":
                    if i > 0:
                        i -= 1
                        await msg.edit(embed = embeds[i])
                elif reaction == "▶":
                    if i < len(embeds) - 1:
                        i += 1
                        await msg.edit(embed = embeds[i])
                elif reaction == "⏭":
                    if i < len(embeds) - 1:
                        i = len(embeds) - 1
                        await msg.edit(embed = embeds[i])
                elif reaction == "✅":
                    await msg.delete()
                    return
            try:
                reaction, user = await self.wait_for('reaction_add',
                    timeout=30.0, check = lambda _, user: user.id == msg_author_id)
                await msg.remove_reaction(reaction, msg_author)
                reaction = str(reaction)
            except TimeoutError:
                break

    async def send_invalid_embed(self, error_message, message):
        invalid_embed = create_invalid_embed(error_message)
        await message.channel.send(embed = invalid_embed, delete_after = 20.0)

if __name__ == "__main__":
    from discord import Intents
    intents = Intents.default()
    intents.members = True
    client = MyClient()
    client.run(get_token())

# git reset --soft HEAD~1
# pip install --pre gql[all]
# https://github.com/graphql-python/gql
# https://bybit-exchange.github.io/docs/inverse/#t-query_liqrecords
