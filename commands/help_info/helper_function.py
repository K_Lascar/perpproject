from discord import Embed

def create_main_help_embed():
    from commands.perpetual.helper import random_hex_colour
    pages = 4
    title = f"Perpetual Community Bot's Commands (Page 1/{pages})"
    description = f"Perpetual Protocol Community Bot was developed for the PERP Community. Enjoy!"
    icon_url="https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png"
    url = "https://perp.fi/"

    help_embed_1 = Embed(title = title, description = description, colour = random_hex_colour())
    help_embed_1.set_author(name = "Perpetual Protocol", url = url,
        icon_url=icon_url)
    help_embed_1.set_thumbnail(url = icon_url)

    socials_title = "📸 Social Media"
    socials_desc = "For more info about PERP's socials.\n"
    socials_desc += "**Usage:** `perp socials`\n"
    help_embed_1.add_field(name = socials_title, value = socials_desc)

    markets_title = "🏦 Exchanges"
    markets_desc = "For more info about the exchanges PERP is available on.\n"
    markets_desc += "**Usage:** `perp exchanges`\n"
    help_embed_1.add_field(name = markets_title, value = markets_desc, inline=False)

    total_title = "💵 Funding"
    total_desc = "For more info about the funding of AMMs on the Perp Exchange.\n"
    total_desc += "**Usage:** `funding` (list funding for all AMMs)\n"
    total_desc += "**Usage Specific:** `funding perp`\n"
    total_desc += "**Info:** `perp help funding`\n"
    help_embed_1.add_field(name = total_title, value = total_desc, inline=False)
    help_embed_1.set_footer(text = "Perpetual Protocol Community Bot", icon_url = icon_url)

    trade_title = "🤝 Trades"
    trade_desc = "For more info about the most recent trades for PERP.\n"
    trade_desc += "**Usage:** `perp trades`\n"
    help_embed_1.add_field(name = trade_title, value = trade_desc, inline=False)


    title = f"Perpetual Community Bot's Commands (Page 2/{pages})"
    help_embed_2 = Embed(title = title, description = description, colour = random_hex_colour())
    help_embed_2.set_thumbnail(url = icon_url)
    help_embed_2.set_author(name = "Perpetual Protocol", url = url,
        icon_url=icon_url)

    price_title = "💲 Price"
    price_desc = "To retrieve the current price of PERP.\n"
    price_desc += "**Usage:** `p perp`\n"
    help_embed_2.add_field(name = price_title, value = price_desc, inline=False)

    info_title = "📋 Information"
    info_desc = "For more info about PERP on the fundamental level.\n"
    info_desc += "**Usage:** `info perp`\n"
    help_embed_2.add_field(name = info_title, value = info_desc, inline=False)
    help_embed_2.set_footer(text ="Perpetual Protocol Community Bot" , icon_url = icon_url)

    trading_title = "📈 Tradingview Charts"
    trading_desc = "To retrieve the TradingView chart of tickers.\n"
    trading_desc += "**Usage:** `c perp`\n"
    trading_desc += "**Advanced Usage:** `c perp 4h rsi macd ichi`\n"
    trading_desc += "**Info:** `perp help c`\n"
    help_embed_2.add_field(name = trading_title, value = trading_desc, inline=False)

    volume_title = "🔊 Trading Volume"
    volume_desc = "To retrieve the trading volume of PERP on Sushiswap and Uniswap.\n"
    volume_desc += "**Volume Usage:** `perp volume (days optional)`\n"
    volume_desc += "**Info:** `perp help volume`\n"
    help_embed_2.add_field(name = volume_title, value = volume_desc, inline=False)
    help_embed_2.set_footer(text = "Perpetual Protocol Community Bot", icon_url = icon_url)


    title = f"Perpetual Community Bot's Commands (Page 3/{pages})"

    help_embed_3 = Embed(title = title, description = description, colour = random_hex_colour())

    title = "💧 Liquidity"
    desc="To retrieve the trading volume of PERP on Sushiswap and Uniswap.\n"
    desc += "**Liquidity Usage:** `perp liquidity (days optional)`\n"
    desc += "**Info:** `perp help liquidity`\n"
    help_embed_3.add_field(name = title, value = desc, inline=False)
    help_embed_3.set_footer(text = "Perpetual Protocol Community Bot", icon_url = icon_url)

    staking_title = "🥩 Staking"
    staking_desc = "To retrieve the staking details for an address.\n"
    staking_desc += "**Usage:** `staking 0xaddr` (0xaddr is an address)\n"
    staking_desc += "**Vesting Usage:** `staking vesting 0xaddr`\n"
    staking_desc += "**Info:** `perp help staking`\n"
    help_embed_3.add_field(name = staking_title, value = staking_desc, inline=False)

    leaderboard_title = "🏆 Leaderboard"
    leaderboard_desc = "To retrieve the top 5 traders on the Perp Platform.\n"
    leaderboard_desc += "**Usage:** `leaderboard`\n"
    help_embed_3.add_field(name = leaderboard_title, value = leaderboard_desc, inline=False)

    title = "🧐 Trader Profile"
    desc = "To retrieve the trading performance, most traded AMM and holdings\n"
    desc += "**Usage:** `profile 0xaddr (0xaddr is an address)`\n"
    desc += "**Info:** `perp help profile`\n"
    help_embed_3.add_field(name = title, value = desc, inline=False)
    help_embed_3.set_footer(text = "Perpetual Protocol Community Bot", icon_url = icon_url)

    title = f"Perpetual Community Bot's Commands (Page 4/{pages})"

    help_embed_4  = Embed(title = title, description = description, colour = random_hex_colour())

    title = "💸 Funding Chart"
    desc = "To retrieve the funding charted overtime for different AMMs.\n"
    desc += "**Usage:** `fc (AMM name)`\n"
    desc += "**Example:** `fc perp`\n"
    # desc += "**Info:** `perp help fc` \n"
    help_embed_4.add_field(name = title, value = desc, inline=False)

    title = "🩸 Liquidations Chart"
    desc = "To retrieve the liquidations charted overtime for different AMMs.\n"
    desc += "**Default Usage:** `liq` (Retrieves Liquidations across all AMMs) \n"
    desc += "**AMM Usage:** `liq perp` \n"
    desc += "**Info:** `perp help liq` \n"
    help_embed_4.add_field(name = title, value = desc, inline=False)

    title = "🤜 Market Bias Chart 🤛"
    desc = "To retrieve the bias, the difference between longs and shorts across AMMs.\n"
    desc += "**Default Usage:** `bias` (Retrieves Market Bias across all AMMs) \n"
    desc += "**AMM Usage:** `bias perp` \n"
    desc += "**Info:** `perp help bias` \n"
    help_embed_4.add_field(name = title, value = desc, inline=False)

    transactions_title = "📄 Transactions"
    transactions_desc = "For more info about the most recent transactions for PERP.\n"
    transactions_desc += "**Use:** `perp transactions`\n"
    help_embed_4.add_field(name = transactions_title, value = transactions_desc, inline=False)
    help_embed_4.set_footer(text = "Perpetual Protocol Community Bot", icon_url = icon_url)


    return [help_embed_1, help_embed_2, help_embed_3, help_embed_4]

def create_staked_embed():
    from discord import Embed
    from commands.perpetual.helper import random_hex_colour
    title = "🥩 Total Staked"
    description = "Retrieves the staking details for an address."
    description = "Retrieves the trading performance, most traded AMM and holdings.\n"
    description += "**Command:** `profile 0xaddress`\n"
    description += "**Example:** `staking vesting 0x4B787e5F53BF90E4EC1FD4B5e4CcFf05BB1a60A9`\n"
    description += "**Another Example:** `staking https://blockscout.com/xdai/mainnet/address/0x4B787e5F53BF90E4EC1FD4B5e4CcFf05BB1a60A9`\n"
    staked_embed = Embed(title = title, description = description, colour = random_hex_colour())
    staked_embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url = "https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png")
    return staked_embed

def create_liquidity_embed():
    from commands.perpetual.helper import random_hex_colour
    title = "💧 Liquidity"
    description = "Retrieves a graph of the liquidity of Perp on Uniswap.\n"
    description += "**Shorthand:** `perp liq`, `perpx liquid`\n"
    description += "**Command:** `perp liquidity (days)`\n"
    description += "**Example:** `perp liquidity week`\n"
    description += "**Another Example:** `perp liquidity 21`\n"

    liquidity_embed = Embed(title = title, description = description, colour = random_hex_colour())
    liquidity_embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url = "https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png")
    return liquidity_embed


def create_profile_embed():
    from commands.perpetual.perp.perp_helper import convert_str_to_hex

    colour = convert_str_to_hex("3ceaaa")

    title = "🧐 Trader Profile"
    description = "Retrieves the trading performance, most traded AMM and holdings.\n"
    description += "**Command:** `profile 0xaddress`\n"
    description += "**Example:** `profile 0x1fff09ff1ce7c78bea4f46b1a4b2fa1d50439ee8`\n"
    description += "**Another Example:** `profile https://blockscout.com/xdai/mainnet/address/0x000000ea89990a17ec07a35ac2bbb02214c50152`\n"
    embed = Embed(title=title, description = description, color = colour)
    return embed

def create_bias_embed():
    from commands.perpetual.perp.perp_helper import convert_str_to_hex

    colour = convert_str_to_hex("3ceaaa")

    title = "🤜 Market Bias Chart 🤛"
    description = "Retrieves the bias, the difference between longs and shorts across AMMs.\n"
    description += "**Command:** `bias (optional days or bias type)`\n"
    description += "**Types:** value (based on notional value), count (based on position count), or ratio (value / count)\n"
    description += "**Default Usage:** `bias`\n"
    description += "**Example:** `bias perp value week`\n"
    description += "**Another Example:** `bias ratio 14`\n"
    embed = Embed(title=title, description = description, color = colour)
    embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url = "https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png")
    return embed

def create_liquidations_embed():
    from commands.perpetual.perp.perp_helper import convert_str_to_hex

    colour = convert_str_to_hex("3ceaaa")

    title = "🩸 Liquidations Chart"
    description = "Retrieves liquidations overtime across AMMs.\n"
    description += "**Command:** `liq (optional days or amm)`\n"
    description += "**Example:** `liq`\n"
    description += "**Another Example:** `liq btc`\n"
    embed = Embed(title=title, description = description, color = colour)
    embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url = "https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png")
    return embed

def create_tv_chart_embed():
    from discord import Embed
    from commands.perpetual.helper import random_hex_colour
    title = "📈 Tradingview Charts"
    description = "Retrieves a chart from Tradingview.\n"
    description += "**Command:** `c (symbol) (indicators and/or timeframes)`\n"
    description += "**Example:** `c perp day moonphase`\n"
    description += "**Another Example:** `c perp macd 4h env, c perp ma moonphase, c perp 30m moon pivot wide`\n"

    tv_embed = Embed(title = title, description = description, colour = random_hex_colour())
    tv_embed.set_thumbnail(url = "https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png")
    tv_embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url = "https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png")
    return tv_embed

def create_volume_embed():
    from discord import Embed
    from commands.perpetual.helper import random_hex_colour
    title = "🔊 Trading Volume"
    description = "Retrieves a graph of the trading volume of Perpetual Protocol on Uniswap.\n"
    description += "**Shorthand:** `perp vol`\n"
    description += "**Command:** `perp volume (timeframe)`\n"
    description += "**Example:** `perp volume week`\n"
    description += "**Another Example:** `perp volume 14`\n"

    volume_embed = Embed(title = title, description = description, colour = random_hex_colour())
    volume_embed.set_thumbnail(url = "https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png")
    volume_embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url = "https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png")
    return volume_embed

def create_help_info():
    from discord import Embed
    from commands.perpetual.helper import random_hex_colour
    help_title = "Perpetual Community Bot"
    description = "Hi this is Perpetual Community Bot.\n"
    description += "To prevent spam the bot will only be used in this channel.\n"
    description += "To get started, please use the command `perp help`.\n"
    description += "If you find any bugs please contact me <@249079413696757760>!\n"
    icon_url="https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png"
    help_embed = Embed(title = help_title, description = description, colour = random_hex_colour())
    help_embed.set_author(name = "Perpetual Protocol", url = "https://perp.fi/",
        icon_url=icon_url)
    help_embed.set_thumbnail(url = icon_url)
    help_embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url = icon_url)
    return help_embed