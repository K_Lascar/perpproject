
def create_social_embed():
    from discord import Embed
    from random import randint
    from commands.perpetual.helper import random_hex_colour
    description = ["Mind helping us grow?",
    "Follow us to get the latest updates!",
    "Please share with friends!"][randint(0,2)]
    social_embed = Embed(title = "Socials",
    description = description, color = random_hex_colour())
    icon_url = "https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png"
    social_embed.set_author(name = "Perpetual Protocol", icon_url = icon_url)

    # https://stackoverflow.com/a/62847744
    # https://stackoverflow.com/a/65133341

    # For separating emote with link.
    factor = 4
    link_desc = "\u200b " * factor + "\n"
    link_desc += "<:twitter:818703339414880276>" + " \u200b " * factor + "[Twitter](https://twitter.com/perpprotocol)\n\n"
    link_desc += "<:telegram:818703370490609695>" + " \u200b " * factor + "[Telegram](https://t.me/perpetualprotocol)\n\n"
    link_desc += "<:github:818703310001274892>" + " \u200b " * factor + "[Github](https://github.com/perpetual-protocol/)\n\n"
    link_desc += "<:medium:818703273795911726>" + " \u200b " * factor + "[Medium](https://medium.com/@perpetualprotocol)\n\n"
    link_desc += " \u200b " * factor + "\n"
    social_embed.add_field(name = "Links", value = link_desc)
    social_embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url=icon_url)
    return social_embed
