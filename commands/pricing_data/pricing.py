from concurrent.futures import ThreadPoolExecutor
from .crypto import is_cryptocurrency, get_info_crypto, get_crypto_price


# Useful Command: git reset --soft HEAD~1
def get_pricing(symbol):
    executor = ThreadPoolExecutor(max_workers = 1)

    is_crypto = executor.submit(is_cryptocurrency, symbol).result()

    if is_crypto:
        crypto, currency = is_crypto["crypto"], is_crypto["currency"]
        return get_crypto_price(crypto, currency)

    error_message = f"I could not find any price information for *{symbol}*, " + \
        "please provide a valid symbol."
    return create_invalid_embed(error_message)

def get_info(symbol):
    executor = ThreadPoolExecutor(max_workers = 1)
    is_crypto = executor.submit(is_cryptocurrency, symbol).result()

    if is_crypto:
        crypto, currency = is_crypto["crypto"], is_crypto["currency"]
        return get_info_crypto(crypto, currency)

    error_message = f"I could not find any information for *{symbol}*, " + \
        "please provide a valid ticker/symbol."
    return create_invalid_embed(error_message)


def create_invalid_embed(error_message):
    from discord import Embed
    icon_url = "https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png"
    invalid_embed = Embed(title = error_message, color = 0xff3d0d)
    invalid_embed.set_author(name = "Data unavailable", icon_url = icon_url)
    return invalid_embed


# Used for adding new cryptos, and images into database.
# def get_image_data(cg, crypto_id):
#     crypto_data = cg.get_coin_by_id(id = crypto_id, localization = "false",
#         tickers = "false", market_data = "false", community_data = "false",
#         developer_data = "false")
#     return crypto_data["image"]["large"]

# async def add_new_coins():
#     from pycoingecko import CoinGeckoAPI
#     from sqlalchemy import create_engine
#     from pandas import DataFrame
#     from time import sleep
#     cg = CoinGeckoAPI()
#     count = 0
#     new_engine = create_engine("sqlite:///discord.db?check_same_thread=False")
#     for coin in cg.get_coins_list():
#         if not check_dupe(new_engine, coin["id"]) and coin["id"]:
#             print(coin)
#             coin["symbol"] = coin["symbol"].lower()
#             coin["icon_url"] = get_image_data(cg, coin["id"])
#             df = DataFrame(coin, index = [0])
#             df.to_sql("cryptos", new_engine, if_exists="append", index=False)
#             count += 1
#             # To avoid ratelimit
#             if count == 8:
#                 sleep(7)
#                 count = 0
#     print("FINISHED SYNC NEW COINS")

# def check_dupe(engine, id):
#     # INDEX
#     return engine.execute("""
#         SELECT id
#         FROM cryptos
#         WHERE id == ?""", id).fetchone() != None

# async def add_top_coins():
#     from pycoingecko import CoinGeckoAPI
#     from sqlalchemy import create_engine
#     engine = create_engine("sqlite:///discord.db?check_same_thread=False")
#     top_cryptos = engine.execute("SELECT symbol FROM TOP_CRYPTOS").fetchall()
#     cg = CoinGeckoAPI()
#     data = cg.get_coins_markets(vs_currency = "usd", order = "market_cap_desc",
#     per_page = "100")
#     for index, crypto in enumerate(data):
#         symbol = crypto["symbol"]
#         if engine.execute("""SELECT symbol
#         FROM TOP_CRYPTOS
#         WHERE ROWID == ?
#         """, index + 1).fetchone()[0] != top_cryptos[index][0]:
#             engine.execute("""UPDATE TOP_CRYPTOS
#                     SET symbol = ?
#                     WHERE rowid == ?""", symbol, index + 1)
#     print("FINISHED SYNC TOP COINS")
