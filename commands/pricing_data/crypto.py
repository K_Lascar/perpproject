from pycoingecko import CoinGeckoAPI
from discord import Embed
from concurrent.futures import ThreadPoolExecutor
from commands.perpetual.helper import random_hex_colour, get_currency_symbol
from requests import get
def get_crypto_percent_change(change_24h, change_7d, change_30d, change_1y,
    currency):
    percentage_change_info = ""
    if change_24h:
        change_24h = change_24h[currency]
        percentage_change_info += f"Past day: {change_24h}%\n"
    if change_7d:
        change_7d = change_7d[currency]
        percentage_change_info += f"Past week: {change_7d}%\n"
    if change_30d:
        change_30d = change_30d[currency]
        percentage_change_info += f"Past month: {change_30d}%\n"
    if change_1y:
        change_1y = change_1y[currency]
        percentage_change_info += f"Past year: {change_1y}%\n"
    return percentage_change_info

def create_crypto_info_embed(ath, atl, change_24h, change_7d, change_30d,
    change_1y, circulating_supply, crypto_id, crypto_name, crypto_symbol,
    currency, current_price, homepage, image_url, market_cap_value, rank,
    total_supply, total_volume):
    title = f"{crypto_name} ({crypto_symbol.upper()})"

    if not rank:
        description = f"There is no rank available for **{crypto_symbol.upper()}**."
    else:
        description = f"Market Cap Ranking: #{rank}."
    crypto_info_embed = Embed(title = title, description = description,
        url = homepage,colour = random_hex_colour())
    crypto_info_embed.set_thumbnail(url = image_url)

    details_desc = get_crypto_field_details(circulating_supply, crypto_id,
        crypto_symbol, currency, market_cap_value, total_volume, total_supply)
    crypto_info_embed.add_field(name = "Details", value = details_desc, inline = False)

    ath = format_price(ath)
    atl = format_price(atl)

    current_price_info = f"Current: $"
    if current_price - int(current_price) == 0:
        current_price_info += f"{current_price:,.2f}"
    else:
        current_price_info += f"{current_price:,.4f}"
    current_price_info += f" {currency.upper()}\n"
    current_price_info += f"ATH: ${ath} {currency.upper()}\n"
    current_price_info += f"ATL: ${atl} {currency.upper()}\n"
    crypto_info_embed.add_field(name = "Price", value = current_price_info,
        inline = True)

    percentage_change_info = get_crypto_percent_change(change_24h, change_7d,
        change_30d, change_1y, currency)
    crypto_info_embed.add_field(name = "Percentage Change", value = percentage_change_info,
        inline = True)

    crypto_info_embed.set_footer(text = "Data provided by CoinGecko.")
    return crypto_info_embed

def create_crypto_price_embed(currency, icon_url, percent_change, price, symbol):
    title = get_currency_symbol(currency)

    title +=  f"{price} {currency.upper()} *({round(percent_change, 2)}%)*"
    crypto_embed = Embed(title = title,colour = random_hex_colour())
    crypto_embed.set_author(name = symbol.upper() + currency.upper(),
    icon_url = icon_url)
    crypto_embed.set_footer(text = "Data provided by CoinGecko")
    return crypto_embed

def get_crypto_field_details(circulating_supply, crypto_id, crypto_symbol,
    currency, market_cap_value, total_volume, total_supply):
    details_desc = ""
    # https://intellipaat.com/community/2447/how-to-print-number-with-commas-as-thousands-separators
    # tessla-coin has an unknown market cap. (Bug in AlphaBot)
    if market_cap_value:
        details_desc += f"Market Cap: ${market_cap_value:,.2f} {currency.upper()}\n"

    # Ethereum has an unknown total supply.
    if total_volume:
        details_desc += f"Total Volume: ${total_volume:,.2f} {currency.upper()}\n"
    if total_supply:
        total_supply = int(total_supply)
        details_desc += f"Total Supply: {total_supply:,d} {crypto_symbol.upper()}\n"
    if circulating_supply:
        circulating_supply = int(circulating_supply)
        details_desc += f"Circulating Supply: {circulating_supply:,d} {crypto_symbol.upper()}\n"
    return details_desc

# https://cdn.betterttv.net/emote/5a2691c6fc6e584787d98534/3x
def get_crypto_price_data(cg, crypto_id, currency, include_mcap, include_change):
    return cg.get_price(ids = crypto_id, vs_currencies = currency, \
                  include_market_cap = include_mcap, \
                  include_24hr_change = include_change)

def get_crypto_image_data(cg, crypto_term, is_id):
    from sqlalchemy import create_engine
    new_engine = create_engine("sqlite:///discord.db")
    if is_id:
        icon_url = new_engine.execute("""SELECT icon_url
                          FROM cryptos
                          WHERE id = ?""", crypto_term).fetchone()
    else:
        icon_url = new_engine.execute("""SELECT icon_url
                          FROM cryptos
                          WHERE symbol = ?""", crypto_term.lower()).fetchone()
    if not icon_url:
        icon_url = "https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png"
    else:
        icon_url = icon_url[0]
    return icon_url

def get_crypto_market_data(cg, crypto_id):
    coin_info = cg.get_coin_by_id(id = crypto_id, localization = "false",
        tickers = "false", community_data = "false", developer_data = "false")
    return coin_info

def find_highest_mcap_crypto(cg, currency, crypto_collection):
    executor = ThreadPoolExecutor(max_workers = 1)
    cap = -1
    for cryptocurrency in crypto_collection:
        price_info = list(executor.submit(get_crypto_price_data, cg,
            cryptocurrency["id"], currency, "true",
            "false").result().values())
        if price_info[0]:
            price_info = price_info[0][f"{currency}_market_cap"]
            if price_info > cap:
                cap = price_info
                crypto = cryptocurrency
    return crypto

def is_cryptocurrency(crypto):
    if crypto.startswith("'") and crypto.endswith("'") or \
        crypto.startswith("‘") and crypto.endswith("’") or \
        crypto.startswith("!"):
        return False

    if crypto.startswith("$"):
        crypto = crypto[1:]

    # Make text lowercase.
    crypto = crypto.lower()
    cg = CoinGeckoAPI()
    currency = "usd"
    index = 0

    # Check if symbol is entire crypto list.
    crypto_collection = list(filter(lambda c: crypto == c["symbol"].lower(),
                    cg.get_coins_list()))

    if crypto_collection:
        crypto = crypto_collection[index]
        if len(crypto_collection) > 1:
            crypto = find_highest_mcap_crypto(cg, currency, crypto_collection)

        return {"currency": currency, "crypto": crypto}

    # If symbol has multiple occurrences on CoinGecko, you can select preference
    # p egt:2
    if crypto[-2:-1] == ":":
        if not crypto[-1].isnumeric():
            return False
        index = int(crypto[-1])
        crypto = crypto[:-2]


    # Find any currency pairs for the crypto mentioned
    currency_pairs = list(filter(lambda c: crypto.endswith(c),
                    cg.get_supported_vs_currencies()))

    # If we have a currency pair modify the new crypto name.
    if currency_pairs:

        currency = currency_pairs[0]
        crypto = "".join(crypto.split(currency))

        if not crypto:
            crypto = currency
            currency = "usd"

    crypto_collection = list(filter(lambda c: crypto.lower() == c["symbol"].lower(),
                    cg.get_coins_list()))

    if not crypto_collection:
        return False
    if index:
        # If index provided is larger than crypto collection, than it's an error.
        if (index > len(crypto_collection)):
            return False
        index -= 1
        crypto = crypto_collection[index]
    else:
        crypto = crypto_collection[index]

        if len(crypto_collection) > 1:
            crypto = find_highest_mcap_crypto(cg, currency, crypto_collection)

    # Might need to cache
    return {"currency": currency, "crypto": crypto}

def get_info_crypto(crypto, currency):
    crypto_id = crypto["id"]
    crypto_name = crypto["name"]
    crypto_symbol = crypto["symbol"]

    cg = CoinGeckoAPI()
    crypto_data = get_crypto_market_data(cg, crypto_id)
    homepage = crypto_data["links"]["homepage"][0]
    market_data = crypto_data["market_data"]
    current_price = market_data["current_price"][currency]
    circulating_supply = market_data["circulating_supply"]
    market_cap_value = market_data["market_cap"][currency]
    market_cap_value = circulating_supply * current_price
    rank = market_data["market_cap_rank"]
    total_volume = market_data["total_volume"][currency]
    atl = market_data["atl"][currency]
    ath = market_data["ath"][currency]
    percent_change_24h = market_data["price_change_percentage_24h_in_currency"]
    percent_change_7d = market_data["price_change_percentage_7d_in_currency"]
    percent_change_30d = market_data["price_change_percentage_30d_in_currency"]
    percent_change_1y = market_data["price_change_percentage_1y_in_currency"]
    image_url = crypto_data["image"]["large"]
    total_supply = market_data["total_supply"]

    return create_crypto_info_embed(ath, atl, percent_change_24h, percent_change_7d,
        percent_change_30d, percent_change_1y, circulating_supply, crypto_id,
        crypto_name, crypto_symbol, currency, current_price, homepage, image_url,
        market_cap_value, rank, total_supply, total_volume)

def format_price(price):
    is_small = True if int(price) < 1 else False
    if is_small:
        return ("%0.8f" % price).rstrip("0")
    return f"{price:,.2f}"

def get_crypto_price(crypto, currency):
    crypto_id = crypto["id"]
    executor = ThreadPoolExecutor(max_workers = 2)
    cg = CoinGeckoAPI()
    price_info = executor.submit(get_crypto_price_data, cg, crypto_id,
                        currency, "false", "true").result()[crypto_id]
    icon_url = executor.submit(get_crypto_image_data, cg, crypto_id, True).result()

    # https://stackoverflow.com/a/49233338/14151099
    price = format_price(price_info[currency])
    percent_change = price_info[currency + '_24h_change']
    if not percent_change:
        percent_change = 0.00
    crypto_embed = create_crypto_price_embed(currency, icon_url, percent_change,
        price, crypto["symbol"])
    return crypto_embed





# Useful commands for later
# def set_value(value):
#     result = "$"
#     if value // (10 ** 12) > 0:
#         value = value / (10 ** 12)
#         result += f"{value:,.3f}T"
#     elif value // (10 ** 9) > 0:
#         value = value / (10 ** 9)
#         result += f"{value:,.3f}B"
#     elif value // (10 ** 6) > 0:
#         value = value / (10 ** 6)
#         result += f"{value:,.3f}M"
#     elif value // (10 ** 3) > 0:
#         value = value / (10 ** 3)
#         result += f"{value:,.3f}"
#     result += " USD"
#     return result

# def process_time(desc, hours, minutes):
#     hour_period = "Hours" if hours > 1 else "Hour"
#     minute_period = "Minutes" if minutes > 1 else "Minute"
#     if hours > 0:
#         desc += f"{hours} {hour_period}"
#         if minutes > 0:
#             desc += ", "
#     if minutes > 0:
#         desc += f"{minutes} {minute_period}"
#     desc += "\n\n"
#     return desc

# def get_human_dates():
#     return {
#         "1": ["daily", "day", "1", "d", "1d"],
#         "7": ["week", "w", "wk", "7 days", "7", "7d"],
#         "14": ["fortnight", "2 week", "2 weeks", "fort", "14 days", "14 d", "14", "f", "14d"],
#         "30": ["month", "monthly", "30 days", "30 d", "30", "m", "30d"],
#         "365": ["year", "yearly", "365", "365 days", "365 day", "52 weeks", "52 w", "52 week", "365d", "52w"]
#     }

# def process_date_arguments(date):
#     human_dates = get_human_dates()
#     for key, value in human_dates.items():
#         if date.lower() in value:
#             return key
#     return False