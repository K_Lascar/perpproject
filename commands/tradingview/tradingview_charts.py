from .charts import create_invalid_chart_embed, is_td_symbol, get_td_chart
from concurrent.futures import ThreadPoolExecutor

def get_valid_timeframes():
    return {
        "1": ["min", "1min", "minute", "1m", "1", "m"],
        "3": ["3min", "3m", "3minutes", "3"],
        "5": ["5min", "5m", "5minutes", "5"],
        "15": ["15min", "15m", "15minutes", "15"],
        "30": ["30min", "30m", "30minutes", "30"],
        "60": ["hr", "1hr", "h", "1hour", "1h", "60", "hourly", "60m"],
        "120": ["2hours", "2hrs", "2hr", "2h", "120"],
        "180": ["3hours", "3hrs", "3hr", "3h", "180"],
        "240": ["4hours", "4hrs", "4hr", "4h", "240"],
        "d": ["d", "day",  "1day", "1d", "daily", "24h", "24hr"],
        "w": ["w", "week", "1week", "1w", "weekly", "7d", "wk"],
        "M": ["mon", "month", "1month", "monthly", "30d", "1mo", "mo", "M"]
    }

def get_valid_indicators():
    return {
        "accd": "ACCD@tv-basicstudies",
        "adr": "studyADR@tv-basicstudies",
        "arn": "AROON@tv-basicstudies",
        "atr": "ATR@tv-basicstudies",
        "awsm": "AwesomeOscillator@tv-basicstudies",
        "bb": "BB@tv-basicstudies",
        "bbr": "BollingerBandsR@tv-basicstudies",
        "bbw": "BollingerBandsWidth@tv-basicstudies",
        "cmf": "CMF@tv-basicstudies",
        "co": "ChaikinOscillator@tv-basicstudies",
        "cmo": "chandeMO@tv-basicstudies",
        "chop": "ChoppinessIndex@tv-basicstudies",
        "cci": "CCI@tv-basicstudies",
        "crsi": "CRSI@tv-basicstudies",
        "cc": "CorrelationCoefficient@tv-basicstudies",
        "dpo": "DetrendedPriceOscillator@tv-basicstudies",
        "dm": "DM@tv-basicstudies",
        "don": "DONCH@tv-basicstudies",
        "2ema": "DoubleEMA@tv-basicstudies",
        "eom": "EaseOfMovement@tv-basicstudies",
        "efi": "EFI@tv-basicstudies",
        "env": "ENV@tv-basicstudies",
        "fish": "FisherTransform@tv-basicstudies",
        "hv": "HV@tv-basicstudies",
        "hull": "hullMA@tv-basicstudies",
        "ichi": "IchimokuCloud@tv-basicstudies",
        "kelt": "KLTNR@tv-basicstudies",
        "kst": "KST@tv-basicstudies",
        "lr": "LinearRegression@tv-basicstudies",
        "macd": "MACD@tv-basicstudies",
        "mom": "MOM@tv-basicstudies",
        "mf": "MF@tv-basicstudies",
        "moonphase": "MoonPhases@tv-basicstudies",
        "ma": "MASimple@tv-basicstudies",
        "ema": "MAExp@tv-basicstudies",
        "wma": "MAWeighted@tv-basicstudies",
        "obv": "OBV@tv-basicstudies",
        "psar": "PSAR@tv-basicstudies",
        "pphl": "PivotPointsHighLow@tv-basicstudies",
        "pip": "PivotPointsStandard@tv-basicstudies",
        "po": "PriceOsc@tv-basicstudies",
        "pivot": "PriceVolumeTrend@tv-basicstudies",
        "roc": "ROC@tv-basicstudies",
        "rsi": "RSI@tv-basicstudies",
        "rvig": "VigorIndex@tv-basicstudies",
        "rvol": "VolatilityIndex@tv-basicstudies",
        "smii": "SMIErgodicIndicator@tv-basicstudies",
        "smio": "SMIErgodicOscillator@tv-basicstudies",
        "stoch": "Stochastic@tv-basicstudies",
        "srsi": "StochasticRSI@tv-basicstudies",
        "tema": "TripleEMA@tv-basicstudies",
        "trix": "Trix@tv-basicstudies",
        "ult": "UltimateOsc@tv-basicstudies",
        "vstop": "VSTOP@tv-basicstudies",
        "vol": "Volume@tv-basicstudies",
        "vwap": "VWAP@tv-basicstudies",
        "vwma": "MAVolumeWeighted@tv-basicstudies",
        "willr": "MAVolumeWeighted@tv-basicstudies",
        "willa": "WilliamsFractal@tv-basicstudies",
        "willf": "WilliamsFractal@tv-basicstudies",
        "zig": "ZigZag@tv-basicstudies"
    }

def check_indicators(indicator_set):
    new_list = []
    indicators = get_valid_indicators()
    set_copy = indicator_set.copy()
    for indicator in indicator_set:
        lower_indicator = indicator.lower()
        if lower_indicator not in indicators.keys():
            continue
        new_list.append(indicators[lower_indicator])
        set_copy.remove(indicator)
    return (new_list, set_copy)

def check_timeframes(timeframe_set):
    timeframes = get_valid_timeframes()
    new_list = []
    for unique_tf in timeframe_set:
        unique_tf = unique_tf.lower()
        if not any(unique_tf in val for val in timeframes.values()):
            error_message = f"Invalid argument `{unique_tf}`, please specify a valid argument!"
            return create_invalid_chart_embed(error_message)
        for tf_key, tf in timeframes.items():
            if unique_tf.lower() in tf:
                new_list.append(tf_key)
                break
    return new_list

def is_top_crypto(symbol):
    from sqlalchemy import create_engine
    new_engine = create_engine("sqlite:///discord.db")
    return new_engine.execute("""SELECT COUNT(*)
            FROM TOP_CRYPTOS
            WHERE symbol == ?
            """, symbol.lower()).fetchone()[0] > 0

def process_symbol(symbol):
    if is_top_crypto(symbol):
        symbol += "usd"
    if symbol.startswith("NATGAS"):
        return "FX_IDC:USDNTG"
    return symbol

def get_chart(symbol, timeframe, indicator_list):
    executor = ThreadPoolExecutor(max_workers = 2)
# FX_IDC:USDNTG NATGAS

    if symbol.startswith("$"):
        symbol = symbol[1:] + "usdt"

    symbol = process_symbol(symbol)

    is_valid_td_symbol = executor.submit(is_td_symbol, symbol).result()
    if not is_valid_td_symbol:
        error_message = "I could not find this particular symbol on TradingView" + \
            ", please try again."
        return create_invalid_chart_embed(error_message)

    new_symbol = ""
    if "prefix" in is_valid_td_symbol.keys():
        new_symbol += is_valid_td_symbol["prefix"] + ":"
    new_symbol += is_valid_td_symbol["symbol"]
    td_link = get_td_chart(new_symbol.upper(), timeframe, indicator_list)
    return td_link
