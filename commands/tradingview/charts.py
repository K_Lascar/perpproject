from urllib.parse import urlencode

def get_formatted_indicators(indicator_list):
    formatted_indicators = indicator_list[0]
    for indicator in indicator_list[1:]:
        formatted_indicators += "\u001f"
        formatted_indicators += indicator
    return formatted_indicators

def get_td_link(symbol, timeframe, indicator_list):
    url_type = "widgetembed" if not symbol.upper() == "VIX" else "chart"

    url = f"https://www.tradingview.com/{url_type}/?"
    url_arguments = {
        "frameElementId": "tradingview_7213f",
        "symbol": symbol,
        "interval": timeframe.upper(),
        "hidesidetoolbar": "1",
        "hidetoptoolbar": "1",
        "symboledit": "0",
        "saveimage": "1",
        "toolbarbg": "f1f3f6",
        "theme": "dark",
        "style": "1",
        "timezone": "exchange",
        "withdateranges": "1",
        "studiesoverrides": {},
        "overrides": {},
        "enabled_features": [],
        "disabled_features": [],
        "locale": "en",
        "utm_source": "www.tradingview.com",
        "utm_medium": "widget_new",
        "utm_campaign": "chart",
        "utm_term": symbol
    }
    if indicator_list:
        url_arguments["studies"] = get_formatted_indicators(indicator_list)

    encoded_args = urlencode(url_arguments)
    url += encoded_args
    return url

# Checks if symbol can be queried on tradingview
def is_td_symbol(symbol):
    from requests import get
    exchange = ""
    if ":" in symbol:
        exchange, symbol = symbol.split(":")
    url = "https://symbol-search.tradingview.com/symbol_search/?"

    url_arguments = {
        "text": symbol,
        "exchange": exchange.upper(),
        "type": "",
        "hl": "1",
        "lang": "en",
        "domain": "production"
    }

    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko/20100101 Firefox/84.0",
        "Accept" : "*/*",
        "Host": "symbol-search.tradingview.com",
    }

    encoded_args = urlencode(url_arguments)
    url += encoded_args
    resp = get(url = url, headers = headers).json()
    if not resp or not resp[0]:
        return False

    resp = resp[0]
    td_symbol = resp["symbol"]
    if td_symbol.startswith("<em>"):
        symbol = td_symbol.replace("<em>", "").replace("</em>", "")
    resp_dict = {
        "symbol": symbol,
    }

    if "prefix" in resp.keys():
        resp_dict["prefix"] = resp["prefix"]
    elif (resp["type"] == "crypto" or resp["type"] == "stock") and "exchange" in resp.keys():
        if " " in resp["exchange"]:
            return False
        resp_dict["prefix"] = resp["exchange"]

    return resp_dict

def get_td_chart(symbol, timeframe, indicator):
    # https://medium.com/swlh/5-ways-to-make-your-python-code-faster-c6878acf59c1
    td_link = get_td_link(symbol, timeframe, indicator)
    return td_link