async def get_dataset():
    from aiohttp import ClientSession
    from time import time
    async with ClientSession() as client:
        headers = {
            "Host": "api.prod.dex.guru",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Origin": "https://dex.guru",
            "Referer": "https://dex.guru/token/0xbc396689893d065f41bc2c6ecbee5e0085233447-eth"
        }
        current_time = int(time())
        early_time = current_time - 7776000
        url = "https://api.prod.dex.guru/v1/tokens/0xbc396689893d065f41bc2c6ecbee5e0085233447-eth/history?begin_timestamp=" + str(early_time) \
            +"&end_timestamp=" + str(current_time) +"&interval=day&currency=USD"
        async with client.get(url = url, headers = headers) as resp:
            return await resp.json()

async def get_generated_id_liquidations(client, headers, url):
    data = """{"operationName":"GetResult","variables":{"query_id":18043},"query":"query GetResult($query_id: Int!, $parameters: [Parameter!]) {\n  get_result(query_id: $query_id, parameters: $parameters) {\n    job_id\n    result_id\n    __typename\n  }\n}\n"}"""
    async with client.post(url = url, headers = headers, data = data) as resp:
        return await resp.json()

async def get_liquidations_data(client, generated_id, headers, url):
    from requests import post
    data = """{"operationName":"FindResultDataByResult","variables":{"result_id":\"""" + generated_id + """\"},"query":"query FindResultDataByResult($result_id: uuid!) {\n  query_results(where: {id: {_eq: $result_id}}) {\n    id\n    job_id\n    error\n    runtime\n    generated_at\n    __typename\n  }\n  get_result_by_result_id(args: {want_result_id: $result_id}) {\n    data\n    __typename\n  }\n}\n"}"""
    async with client.post(url = url, headers = headers, data = data) as resp:
        return await resp.json()

async def get_liquidations():
    from aiohttp import ClientSession
    async with ClientSession as client:
        headers = {
            "Host": "core-hsr.duneanalytics.com",
            "Origin": "https://duneanalytics.com",
            "Referer": "https://duneanalytics.com/",
            "X-Hasura-Api-Key": ""
        }
        url = "https://core-hsr.duneanalytics.com/v1/graphql"
        generated_id = get_generated_id_liquidations(headers, url)
        generated_id = generated_id["data"]["get_result"]["result_id"]
        liquidations = get_liquidations_data(generated_id, headers, url)
        liquidations = liquidations["data"]["view_results"][0]["data"]["supply"]
        return liquidations