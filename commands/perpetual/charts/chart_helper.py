def strip_date_time(date_time_text):
    from datetime import datetime
    datetime_obj = datetime.strptime(date_time_text, "%Y-%m-%dT%H:%M:%S")
    return datetime_obj

# https://stackoverflow.com/a/61330823
def human_decimal_dollar_format(num, pos):
    return "$" + human_decimal_format(num)

# https://stackoverflow.com/a/61330823
def human_decimal_format(num):
    mag = 0
    while abs(num) >= 1000:
        mag += 1
        # Multiplication faster operation by a bit in assembly code.
        num *= 0.0001
    return '%.2f%s' % (num, ['', 'K', 'M', "B", 'T','G', 'P'][mag])

def apr_decimal_format(num, pos):
    return '%.2f%%' % (num * 24 * 365)

def create_image_bytes(fig, width, height):
    from io import BytesIO
    buf = BytesIO()
    fig.set_figheight(height)
    fig.set_figwidth(width)

    fig.savefig(buf, transparent=True, bbox_inches = "tight")
    buf.seek(0)
    return buf

def format_dataset(days, dataset, dataset_type):
    from datetime import datetime
    from matplotlib.dates import date2num
    dates = []
    data_collection = []

    for data in dataset[-days:]:
        data_collection.append(data[dataset_type])
        datetime_obj = datetime.fromtimestamp(data["date"])
        dates.append(datetime_obj)

    dates_num_array = date2num(dates)

    return (dates_num_array, data_collection)

def format_funding_dataset(days, dataset, symbol):
    from numpy import array, datetime64, timedelta64
    from matplotlib.dates import date2num
    dates = []
    funding_collection = []

    symbol = symbol.upper() + "-USD"

    initial_date = datetime64(dataset[0]["time"][:-1], "D")

    for i in range(len(dataset)):
        current_date = datetime64(dataset[i]["time"][:-1], "D")

        # Compare current date in list with the initial date and the specified days.
        if initial_date - timedelta64(days + 1, "D") == current_date:
            break

        if symbol == dataset[i]["ammAddr"]:
            funding_collection.append(dataset[i]["fundingRate"])
            dates.append(datetime64(dataset[i]["time"][:-1]))

    dates_num_array = date2num(dates)
    return (dates_num_array, array(funding_collection))

def get_valid_days():
    return {
        7: ["7d", "7days", "7day", "week", "7"],
        14: ["14d", "14days", "14day", "fortnight", "2weeks", "2week", "14"],
        21: ["21d", "21days", "21day", "3weeks", "3week", "21"],
        28: ["28d", "28days", "28day", "4weeks", "4week", "28"],
        30: ["30d", "30days", "30day", "month", "30"],
    }

def check_days(days_format):

    # Remove Negative Numbers
    if days_format.startswith("-"):
        return False

    valid_days = get_valid_days()
    for key, val in valid_days.items():
        if days_format in val:
            return key
    return False

def format_bias_data(dataset, timeperiod, is_amm_based_search):
    from datetime import datetime, timedelta
    formatted_dict = {}

    initial_date = datetime.strptime(dataset[0]["data"]["day"][:-6],"%Y-%m-%dT%H:%M:%S")
    for data in dataset:
        data_resp = data["data"]
        current_date = data_resp["day"]
        current_date = datetime.strptime(current_date[:-6],"%Y-%m-%dT%H:%M:%S")

        # Break condition, if the length exceeds the specified timeperiod.
        if initial_date - timedelta(days = timeperiod) == current_date:
            break


        market = data_resp["market"]
        if is_amm_based_search and is_amm_based_search != market:
            continue


        position_side =  data_resp["positionside"]
        data_dict = {"position_count": data_resp["positioncount"],
                "position_size": data_resp["positionsize"]}

        # If a new market not in formatted dict add it, and what position side (LONG/SHORT) it is.

        # Else if the found position side not in formatted_dict add it.

        # Else condition if market exists and position side, just add the value to
        # the formatted_dict.
        if not market in formatted_dict.keys():
            formatted_dict[market] = {position_side: data_dict}
        elif not position_side in formatted_dict[market].keys():
            formatted_dict[market][position_side] = data_dict
        else:
            formatted_dict[market][position_side]["position_count"] += data_resp["positioncount"]
            formatted_dict[market][position_side]["position_size"] += data_resp["positionsize"]
    return formatted_dict

def format_bias_key_data(formatted_dataset, key):
    result_long = []
    result_short = []

    for market_bias in formatted_dataset.values():
        long = market_bias["LONG"][key]
        short = market_bias["SHORT"][key]
        summed_bias = long + short
        result_long.append(((100 * long) / summed_bias))
        result_short.append(((100 * short) / summed_bias))
    return (result_long, result_short)


def format_bias_type_data(formatted_dataset, bias_type):
    amms = []
    long_bias = []
    short_bias = []
    if bias_type == "ratio":

        for market, market_bias in formatted_dataset.items():
            long = market_bias["LONG"]
            short = market_bias["SHORT"]

            long_value = long["position_size"]
            short_value = short["position_size"]

            long_count = long["position_count"]
            short_count = short["position_count"]

            long_ratio = long_value / long_count
            short_ratio = short_value / short_count

            long_bias_ratio = long_ratio / (long_ratio + short_ratio)
            short_bias_ratio = short_ratio / (long_ratio + short_ratio)

            long_bias.append((100 * long_bias_ratio))
            short_bias.append((100 * short_bias_ratio))


    elif bias_type == "size":

        long_bias, short_bias = format_bias_key_data(formatted_dataset, "position_size")

    elif bias_type == "count":
        long_bias, short_bias = format_bias_key_data(formatted_dataset, "position_count")

    amms = [market for market in formatted_dataset.keys()]
    return (amms, long_bias, short_bias)