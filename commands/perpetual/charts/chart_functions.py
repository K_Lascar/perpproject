# https://github.com/dhaitz/mplcyberpunk
# https://medium.com/codex/step-by-step-guide-to-data-visualizations-in-python-b322129a1540
# https://stackoverflow.com/a/21884187
def create_dataset_chart(dates, dataset, dataset_type):
    from matplotlib.pyplot import gca, style, plot, title, gcf, clf, \
         close, xlabel, ylabel
    from matplotlib.dates import DateFormatter, AutoDateLocator, ConciseDateFormatter
    from matplotlib.ticker import FuncFormatter
    from mplcyberpunk import add_glow_effects
    from .chart_helper import human_decimal_dollar_format, create_image_bytes

    # FORMATTING STYLE OF CHART
    style.use("cyberpunk")
    locator = AutoDateLocator()
    x_formatter = ConciseDateFormatter(locator)
    y_formatter = FuncFormatter(human_decimal_dollar_format)

    # SET AXIS FORMATS
    axis = gca()
    axis.xaxis.set_major_locator(locator)
    axis.xaxis.set_major_formatter(x_formatter)
    axis.yaxis.set_major_formatter(y_formatter)

    # Hide grid lines
    axis.grid(False)

    title(f"{dataset_type} of Perpetual Protocol in Uniswap",
        color = "white", fontweight = "bold", fontsize=20)
    xlabel("Date", color = "white", fontsize = 15, fontweight = "bold")
    ylabel(dataset_type, color = "white", fontsize = 15, fontweight = "bold")

    # Sparkline chart format
    color = "red" if dataset[0] > dataset[-1] else "cyan"
    plot(dates, dataset, color = color)
    add_glow_effects()
    fig = gcf()
    img_bytes = create_image_bytes(fig, 10, 6)
    # https://stackoverflow.com/q/7101404
    clf()
    close("all")
    return img_bytes

def create_funding_dataset_chart(dates, dataset, amm, time_period):
    from matplotlib.pyplot import gca, style, plot, title, gcf, clf, close, \
        xlabel, ylabel
    from matplotlib.dates import DateFormatter, AutoDateLocator, ConciseDateFormatter
    from matplotlib.ticker import FuncFormatter
    from mplcyberpunk import add_glow_effects
    from .chart_helper import apr_decimal_format, create_image_bytes
    from numpy import array, ma


    # FORMATTING STYLE OF CHART
    style.use("cyberpunk")
    locator = AutoDateLocator()
    x_formatter = ConciseDateFormatter(locator)
    y_formatter = FuncFormatter(apr_decimal_format)

    # SET AXIS FORMATS
    axis = gca()
    axis.xaxis.set_major_locator(locator)
    axis.xaxis.set_major_formatter(x_formatter)
    axis.yaxis.set_major_formatter(y_formatter)

    # https://stackoverflow.com/a/45149018
    # Hide grid lines
    axis.grid(False)

    # FILTER VALUES OUT BELOW 0
    long_trades = ma.masked_less(dataset, 0)
    # FILTER VALUES OUT ABOVE 0
    short_trades = ma.masked_greater(dataset, 0)

    title(f"Funding for {amm['id'].upper()} in {time_period} days",
        color = "white", fontweight = "bold", fontsize=20)
    xlabel("Date", color = "white", fontsize = 15, fontweight = "bold")
    ylabel("Funding Rate APR", color = "white", fontsize = 15, fontweight = "bold")
    color = "white"
    plot(dates, dataset, color = color)

    # Add colour to long trades
    plot(dates, long_trades, color = "cyan")
    # Add colour to short trades
    plot(dates, short_trades, color = "red")

    add_glow_effects()
    fig = gcf()
    img_bytes = create_image_bytes(fig, 10, 6)
    clf()
    close("all")
    return img_bytes

def create_bias_collection_chart(amms, day_count, long_positions, short_positions):
    from matplotlib.pyplot import gca, subplots, title, gcf, clf, \
        close, xlabel, ylabel, barh, rcParams
    from matplotlib.ticker import FuncFormatter
    from .chart_helper import create_image_bytes
    from numpy import array, ma
    from commands.perpetual.perp.perp_helper import convert_str_to_hex

    # Font size for X-Y axis values.
    rcParams.update({"font.size" : 17})
    fig, ax = subplots()

    # https://stackoverflow.com/a/45149018
    # Hide grid lines
    ax.grid(False)
    width = 0.75
    # Long positions bar charts.
    ax.barh(
        amms,
        long_positions,
        width,
        color = "cyan"
    )

    # Short positions bar charts.
    ax.barh(
        amms,
        short_positions,
        width,
        color = "red",
        left = long_positions
    )

    ax.tick_params(colors = "white", which = "both")

    day_type = "Days" if day_count > 1 else "Day"
    colour = "white"

    title(f"Market Bias for {day_count} {day_type}", color = colour,
        fontweight = "bold", fontsize=20)
    ylabel("AMMs", color = "white", fontsize = 20, fontweight = "bold")
    xlabel("Ratio of Longs/Shorts %", color = colour, fontsize = 20,
        fontweight = "bold")

    rects = ax.patches

    # https://stackoverflow.com/a/28931750
    for rect, long, short in zip(rects, long_positions, short_positions):
        # Set Long Bar chart Value
        ax.text(
            long / 2, rect.get_y() + 0.2, "{:.2f} %".format(long), ha="center", va = "baseline",
            fontweight = "extra bold", fontsize = 14, color = "black"
        )
        # Set Short Bar chart Value
        ax.text(
            short / 2 + long, rect.get_y() + 0.2, "{:.2f} %".format(short), ha="center",
            fontweight = "extra bold", fontsize = 14, color = "black"
        )

    img_bytes = create_image_bytes(fig, 10, 12)
    clf()
    close("all")
    return img_bytes

async def get_liquidity_chart(days):
    from .chart_data import get_dataset
    from .chart_helper import format_dataset
    liquidity_data = await get_dataset()
    liquidity_data = liquidity_data["data"]
    dates, liquidity = format_dataset(days, liquidity_data, "totalLiquidityUSD")
    img_bytes = create_dataset_chart(dates, liquidity, "Liquidity")
    return img_bytes

async def get_volume_chart(days):
    from .chart_data import get_dataset
    from .chart_helper import format_dataset
    volume_data = await get_dataset()
    volume_data = volume_data["data"]
    dates, volume = format_dataset(days, volume_data, "dailyVolumeUSD")
    img_bytes = create_dataset_chart(dates, volume, "Volume")
    return img_bytes

def get_funding_chart(funding_data, amm, time_period):
    from .chart_helper import format_funding_dataset
    dates, funding_rate_collection  = format_funding_dataset(time_period,
        funding_data, amm["id"])
    img_bytes = create_funding_dataset_chart(dates, funding_rate_collection, amm, time_period)
    return img_bytes

def get_market_bias_collection_chart(bias_dataset, time_period, bias_type):
    from .chart_helper import format_bias_data, format_bias_type_data
    bias_dataset = format_bias_data(bias_dataset, time_period, False)
    amms, long_bias, short_bias = format_bias_type_data(bias_dataset, bias_type)
    img_bytes = create_bias_collection_chart(amms, time_period, long_bias, short_bias)
    return img_bytes
