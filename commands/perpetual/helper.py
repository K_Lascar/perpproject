from random import randint
# Found from: https://www.color-hex.com/
def random_hex_colour():
    return [
        0xee22cc, 0x3bbade, 0xc44236, 0x5fbeff, 0xff1daa, 0x6bd13e, 0x71cc98,
        0xd9d9d6, 0xaf7dfd, 0xa38ef6, 0xfff68f, 0x96ceb4, 0x9933ff, 0xff9f10,
        0xce3746, 0x37b76a, 0x00ffff, 0xffffff, 0xffa600, 0x003f5c, 0xaaaaaa
    ][randint(0, 20)]

def random_str_colour():
    return [
        "aliceblue","antiquewhite","aqua","aquamarine","azure",
        "beige","bisque","black","blanchedalmond","blue",
        "blueviolet","brown","burlywood","cadetblue",
        "chartreuse","chocolate","coral","cornflowerblue",
        "cornsilk","crimson","cyan","darkblue","darkcyan",
        "darkgoldenrod","darkgray","darkgrey","darkgreen",
        "darkkhaki","darkmagenta","darkolivegreen","darkorange",
        "darkorchid","darkred","darksalmon","darkseagreen",
        "darkslateblue","darkslategray","darkslategrey",
        "darkturquoise","darkviolet","deeppink","deepskyblue",
        "dimgray","dimgrey","dodgerblue","firebrick",
        "floralwhite","forestgreen","fuchsia","gainsboro",
        "ghostwhite","gold","goldenrod","gray","grey","green",
        "greenyellow","honeydew","hotpink","indianred","indigo",
        "ivory","khaki","lavender","lavenderblush","lawngreen",
        "lemonchiffon","lightblue","lightcoral","lightcyan",
        "lightgoldenrodyellow","lightgray","lightgrey",
        "lightgreen","lightpink", "lightseagreen",
        "lightskyblue","lightslategray","lightslategrey",
        "lightsteelblue","lightyellow","lime","limegreen",
        "linen","magenta","maroon","mediumaquamarine",
        "mediumblue","mediumorchid","mediumpurple",
        "mediumseagreen","mediumslateblue","mediumspringgreen",
        "mediumturquoise","mediumvioletred","midnightblue",
        "mintcream","mistyrose","moccasin","navajowhite","navy",
        "oldlace","olive","olivedrab","orange","orangered",
        "orchid","palegoldenrod","palegreen","paleturquoise",
        "palevioletred","papayawhip","peachpuff","peru","pink",
        "plum","powderblue","purple","rosybrown",
        "royalblue","rebeccapurple","saddlebrown",
        "sandybrown","seagreen","seashell","sienna","silver",
        "skyblue","slateblue","slategray","slategrey","snow",
        "springgreen","steelblue","tan","teal","thistle",
        "turquoise","violet","wheat","white","whitesmoke",
        "yellow","yellowgreen"
    ][randint(0, 143)]

def get_currency_symbol(currency):
    if currency == "gbp":
        return "£"
    elif currency == "jpy" or currency == "cny":
        return "¥"
    elif currency == "eur":
        return "€"
    elif currency == "krw":
        return "₩"
    return"$"