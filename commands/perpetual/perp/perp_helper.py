def add_wallet_type(value):
    desc = ""
    if value >= 1000000:
        desc += "<a:peperocket:819883440756359170>"
    if value >= 100000:
        desc += "<a:moneyrain:820063180566429757>"
    elif value >= 50000:
        desc += "🐳"
    elif value >= 10000:
        desc += "🐬"
    elif value >= 5000:
        desc += "🐠"
    desc += "\n"
    return desc

def process_time(desc, hours, minutes, seconds):
    hour_period = "Hours" if hours > 1 else "Hour"
    minute_period = "Minutes" if minutes > 1 else "Minute"
    second_period = "Seconds" if seconds > 1 else "Second"

    if hours > 0:
        desc += f"{hours} {hour_period}"
        if minutes > 0 or seconds > 0:
            desc += ", "
    if minutes > 0:
        desc += f"{minutes} {minute_period}"
        if seconds > 0:
            desc += " and "
    if seconds > 0:
        desc += f"{seconds} {second_period}"
    desc += "\n\n"
    return desc

def splice_time(current_time, timestamp):
    from datetime import datetime
    datetime_obj = datetime.fromtimestamp(timestamp)
    time_diff = current_time - datetime_obj
    hours, remainder = divmod(time_diff.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    time_desc = "**Time ago:** "
    return process_time(time_desc, hours, minutes, seconds)

def get_transaction_url(tx_id):
    url = "https://etherscan.io/tx/"
    return url + tx_id

def get_account_url(address):
    url = f"https://blockscout.com/xdai/mainnet/address/{address}/transactions"
    return url

def get_trader_url(address):
    url = f"https://perpterminal.com/account/{address}"
    return url

def get_funding_from_token(token, funding_data):
    result = []
    count = 0
    for data in funding_data:
        if data["ammAddr"] == token:
            result.append(data)
            count += 1
        if count == 2:
            break

    return result

def get_funding_from_tokens(funding_data):
    from .perp_functions import format_price
    result = ""
    base_time = funding_data[0]["time"][:-14]
    for data in funding_data:
        if data["time"][:-14] == base_time:
            token_name = data['ammAddr'][:-5]
            apr = data['fundingRate'] * 24 * 365
            funding_rate = format_price(data['fundingRate'])
            result += f"**{token_name}** : {funding_rate}% ({apr:.2f}% APR)\n"
        else:
            break
    return result

def format_funding_tokens(funding_rate_lists, symbol_lists, query_type):
    result = ""
    length = len(funding_rate_lists)
    for i in range(length):
        funding_rate = float(funding_rate_lists[i][18:])

        apr = funding_rate * 365 * 24

        long_case = (query_type == "long" and apr < 0)
        short_case = (query_type == "short" and apr > 0)
        if not query_type or long_case or short_case:
            symbol = symbol_lists[i][:-5]
            url = f"https://perp.exchange/t/{symbol.upper()}:USDC"
            result += f"**[{symbol}]({url})** : "
            if abs(apr) > 200:
                result += f"**{funding_rate:.4f} % (APR: {apr:.4f} %)**\n"
            else:
                result += f"{funding_rate:.4f} % (APR: {apr:.4f} %)\n"
    return result

def process_funding_rate(funding_rate):
    from .perp_functions import format_price
    if len(funding_rate) > 1:
        first_rate = funding_rate[0]
        second_rate = funding_rate[1]
        change_rate = first_rate["fundingRate"] - second_rate["fundingRate"]
        change_rate /= second_rate["fundingRate"]
        change_rate *= 100
        title = f"Funding Rate: {format_price(first_rate['fundingRate'])}%"
        if change_rate > 0:
            title += f" (1h: +{change_rate:.2f}%)\n"
        else:
            title += f" (1h: {change_rate:.2f}%)\n"
    else:
        first_rate = funding_rate[0]
        basic_rate = first_rate["fundingRate"]
        title = f"Funding Rate: {basic_rate}\n"
    return (first_rate, title)

def lookup_amm_address(amm_address):
    if amm_address == '0x0f346e19f01471c02485df1758cfd3d624e399b4':
        return "BTC"
    elif amm_address == "0x8d22f1a9dce724d8c1b4c688d75f17a2fe2d32df":
        return "ETH"
    elif amm_address == "0xd41025350582674144102b74b8248550580bb869":
        return "YFI"
    elif amm_address == "0x6de775aabeeede8efdb1a257198d56a3ac18c2fd":
        return "DOT"
    elif amm_address == "0xb397389b61cbf3920d297b4ea1847996eb2ac8e8":
        return "SNX"
    elif amm_address == "0x80daf8abd5a6ba182033b6464e3e39a0155dcc10":
        return "LINK"
    elif amm_address == "0x16a7ecf2c27cb367df36d39e389e66b42000e0df":
        return "AAVE"
    elif amm_address == "0xf559668108ff57745d5e3077b0a7dd92ffc6300c":
        return "SUSHI"
    elif amm_address == "0x33fbaefb2dcc3b7e0b80afbb4377c2eb64af0a3a":
        return "COMP"
    elif amm_address == "0x922f28072babe6ea0c0c25ccd367fda0748a5ec7":
        return "REN"
    elif amm_address == "0xfcae57db10356fcf76b6476b21ac14c504a45128":
        return "PERP"
    elif amm_address == "0xeac6cee594edd353351babc145c624849bb70b11":
        return "UNI"
    elif amm_address == "0xab08ff2c726f2f333802630ee19f4146385cc343":
        return "CRV"
    elif amm_address == "0xb48f7accc03a3c64114170291f352b37eea26c0b":
        return "MKR"
    elif amm_address == "0x7b479a0a816ca33f8eb5a3312d1705a34d2d4c82":
        return "CREAM"
    elif amm_address == "0x187c938543f2bde09fe39034fe3ff797a3d35ca0":
        return "GRT"
    elif amm_address == "0x26789518695b56e16f14008c35dc1b281bd5fc0e":
        return "ALPHA"
    elif amm_address == "0x838b322610bd99a449091d3bf3fba60d794909a9":
        return "FTT"
    return False

def lookup_symbol(symbol):
    if symbol in ["DOT", "DOT-USDC", "DOTUSDC"]:
        return {"address": "0x6de775aaBEEedE8EFdB1a257198d56A3aC18C2FD",
        "symbol": "polkadot", "id": "dot"}
    elif symbol in ["REN", "REN-USDC", "RENUSDC"]:
        return {"address": "0x922F28072BaBe6EA0C0c25cCD367Fda0748a5EC7",
        "symbol": "republic-protocol", "id": "ren"}
    elif symbol in ["AAVE", "AAVE-USDC", "AAVEUSDC"]:
        return {"address": "0x16A7ECF2c27Cb367Df36d39e389e66B42000E0dF",
        "symbol": "aave", "id": "aave"}
    elif symbol in ["SNX", "SNX-USDC", "SNXUSDC"]:
        return {"address": "0xb397389B61cbF3920d297b4ea1847996eb2ac8E8",
        "symbol": "havven", "id": "snx"}
    elif symbol in ["SUSHI", "SUSHI-USDC", "SUSHIUSDC"]:
        return {"address": "0xF559668108Ff57745D5e3077B0A7Dd92FFc6300c",
        "symbol": "sushi", "id": "sushi"}
    elif symbol in ["LINK", "LINK-USDC", "LINKUSDC"]:
        return {"address": "0x80DaF8ABD5a6Ba182033B6464e3E39A0155DCC10",
        "symbol": "chainlink", "id": "link"}
    elif symbol in ["YFI", "YFI-USDC", "YFIUSDC"]:
        return {"address": "0xd41025350582674144102B74B8248550580bb869",
        "symbol": "yearn-finance", "id": "yfi"}
    elif symbol in ["BTC", "BTC-USDC", "BTCUSDC"]:
        return {"address": "0x0f346e19F01471C02485DF1758cfd3d624E399B4",
        "symbol": "bitcoin", "id": "btc"}
    elif symbol in ["ETH", "ETH-USDC", "ETHUSDC"]:
        return {"address": "0x8d22F1a9dCe724D8c1B4c688D75f17A2fE2D32df",
        "symbol": "ethereum", "id": "eth"}
    elif symbol in ["PERP", "PERP-USDC", "PERPUSDC"]:
        return {"address": "0xfcAE57DB10356FCf76B6476B21ac14C504a45128",
        "symbol": "perpetual-protocol", "id": "perp"}
    elif symbol in ["UNI", "UNI-USDC", "UNIUSDC"]:
        return {"address": "0xeaC6CEE594EdD353351BaBc145C624849Bb70b11",
        "symbol": "uniswap", "id": "uni"}
    elif symbol in ["MKR", "MKR-USDC", "MKRUSDC"]:
        return {"address": "0xb48F7aCcc03a3C64114170291F352b37eEa26c0B",
        "symbol": "maker", "id": "mkr"}
    elif symbol in ["CREAM", "CREAM-USDC", "CREAMUSDC"]:
        return {"address": "0x7B479a0a816ca33F8EB5A3312d1705a34d2d4C82",
        "symbol": "cream-2", "id": "cream"}
    elif symbol in ["CRV", "CRV-USDC", "CRVUSDC"]:
        return {"address": "0xAB08fF2c726F2F333802630EE19F4146385CC343",
        "symbol": "curve-dao-token", "id": "crv"}
    elif symbol in ["COMP", "COMP-USDC", "COMPUSDC"]:
        return {"address": "0x33FbaeFb2dCc3B7e0B80afbB4377C2EB64AF0a3A",
        "symbol": "compound-governance-token", "id": "comp"}
    elif symbol in ["GRT", "GRT-USDC", "GRTUSDC"]:
        return {"address": "0x187C938543f2BDE09Fe39034Fe3Ff797A3D35cA0",
        "symbol": "the-graph", "id": "grt"}
    elif symbol in ["ALPHA", "ALPHA-USDC", "ALPHAUSDC"]:
        return {"address": "0x26789518695b56e16F14008c35Dc1b281Bd5fc0E",
        "symbol": "alpha-finance", "id": "alpha"}
    elif symbol in ["FTT", "FTT-USDC", "FTTUSDC"]:
        return {"address": "0x838B322610BD99a449091D3bF3FBA60D794909a9",
        "symbol": "ftx-token", "id": "ftt"}

    return False

def format_open_interest(open_interest_dataset):
    open_interest_dict = {}
    for data in open_interest_dataset:
        market_pair = data["marketPair"]
        if data["marketPair"] in open_interest_dict:
            if data["_side"] == "SHORT":
                open_interest_dict[market_pair].append(data)
            else:
                open_interest_dict[market_pair].insert(0, data)
        else:
            open_interest_dict[market_pair] = [data]
    return open_interest_dict

def format_liquidations(liquidations_dataset):
    from datetime import datetime
    from numpy import array
    formatted_data_collection = []
    for data in liquidations_dataset:
        info = data["data"]
        formatted_data_dict = {}
        formatted_data_dict["x"] = datetime.strptime(info["day"][:-6], "%Y-%m-%dT%H:%M:%S").date()
        formatted_data_dict["y"] = info["positionnotionalliquidated"]
        formatted_data_collection.append(formatted_data_dict)
    return formatted_data_collection

def format_liquidations_by_days(liquidations_dataset, days):
    from datetime import datetime, timedelta
    from numpy import array
    initial_date = datetime.strptime(liquidations_dataset[0]["data"]["day"][:-6], "%Y-%m-%dT%H:%M:%S").date()
    counter = None
    for index in range(1, len(liquidations_dataset)):
        info = liquidations_dataset[index]["data"]
        current_date = datetime.strptime(info["day"][:-6], "%Y-%m-%dT%H:%M:%S").date()
        if initial_date - timedelta(days=8) == current_date:
            counter = index
            break
    return liquidations_dataset[:counter]

def format_liquidations_by_symbol(liquidations_dataset, symbol):
    from datetime import datetime, timedelta
    formatted_data_collection = []
    x_index = 0
    # Looping entire dataset
    while x_index < len(liquidations_dataset):
        x_info = liquidations_dataset[x_index]["data"]
        formatted_data_dict = {}

        x_current_date = datetime.strptime(x_info["day"][:-6], "%Y-%m-%dT%H:%M:%S").date()
        counter = 0
        for y_index in range(x_index, len(liquidations_dataset)):
            y_info = liquidations_dataset[y_index]["data"]

            y_current_date = datetime.strptime(y_info["day"][:-6], "%Y-%m-%dT%H:%M:%S").date()
            # Compare outer loop's current date, if we've exceeded day limit.
            # Break out of the loop.
            if x_current_date - timedelta(days=1) == y_current_date:
                break

            # Case if we have a liquidation for the symbol in the current date.
            if y_info["market"] == symbol.upper():
                formatted_data_dict["x"] = x_current_date
                formatted_data_dict["y"] = round(y_info["positionnotionalliquidated"], 0)
            counter += 1

        # No occurrences occurred during the x_date.
        if not formatted_data_dict:
            formatted_data_dict["x"] = x_current_date
            formatted_data_dict["y"] = 0

        formatted_data_collection.append(formatted_data_dict)
        x_index += counter
    return formatted_data_collection

def rgb_to_hex(rgb):
    rgb = tuple(rgb)
    return "#%02x%02x%02x" % rgb

def get_image_url_dominant_hex_colours(image_url, num_colours):
    from requests import get
    from dominantcolors import get_image_dominant_colors

    # https://stackoverflow.com/a/40944159
    image_path = get_image_path_from_url(image_url)
    dominant_colours = get_image_dominant_colors(image_path, num_colours)
    return [rgb_to_hex(colour) for colour in dominant_colours]

def convert_str_to_hex(hex_string):
    return int("0x" + hex_string, 16)

def get_image_path_from_url(image_url):
    from requests import get
    return get(url = image_url, stream=True).raw

def format_the_graph_number(value):
    return value * 0.000000000000000001

def get_profile_image(address):
    pass

def get_stripped_address(address_link):
    if address_link.endswith("/"):
        address_link = address_link[:-1]

    if (address_link.startswith("https://blockscout.com/xdai/mainnet/address/")):
        result = address_link[len("https://blockscout.com/xdai/mainnet/address/"):]
    elif (address_link.startswith("blockscout.com/xdai/mainnet/address/")):
        result = address_link[len("blockscout.com/xdai/mainnet/address/"):]
    elif (address_link.startswith("https://explorer.anyblock.tools/ethereum/poa/xdai/address/")):
        result = address_link[len("https://explorer.anyblock.tools/ethereum/poa/xdai/address/"):]
    elif address_link.startswith("explorer.anyblock.tools/ethereum/poa/xdai/address/"):
        result = address_link[len("explorer.anyblock.tools/ethereum/poa/xdai/address/"):]
    elif address_link.startswith("https://perpterminal.com/account/"):
        result = address_link[len("https://perpterminal.com/account/"):]
    elif address_link.startswith("perpterminal.com/account/"):
        result = address_link[len("perpterminal.com/account/"):]
    else:
        return address_link

    if result.endswith("/transactions"):
        result = result[:len("/transactions")]

    return result.strip("/")

def lookup_address_in_dune_dataset(address, dune_dataset, query_type):
    address = "\\" + address[1:]
    for data in dune_dataset:
        single_data = data["data"]
        if single_data[query_type] == address:
            return single_data
    return False

def get_formatted_perp_staked_urls(dates_dataset):
    urls = []
    for week, week_data in dates_dataset.items():
        week_hash = week_data["hash"]
        url = "https://s3.amazonaws.com/staking.perp.fi/production/{}/{}.json".format(week, week_hash)
        urls.append(url)
    return urls

def get_total_accrued_rewards(address, staking_dataset):
    total_rewards = 0
    for data in staking_dataset:
        resp = data["resp"]
        if address in resp.keys():
            total_rewards += float(resp[address]["totalRewardPerp"])
    return total_rewards

def get_next_vested_unlock(address, vested_dates_data, vested_staking_data, times):
    from time import time
    result = []
    current_timestamp = int(time())

    counter = 0
    for data in vested_staking_data:
        if counter >= times:
            break

        if int(data["unlock_date"]) > current_timestamp:
            resp = data["resp"]
            week = data["week"]
            if address in resp.keys():
                counter += 1
                reward_dict = {"unlock_date": data["unlock_date"], "resp": resp[address],
                "total_reward_pool": vested_dates_data[week]["totalRewardPerp"],
                "week": week}
                result.append(reward_dict)

    return result