def get_price():
    from pycoingecko import CoinGeckoAPI
    cg = CoinGeckoAPI()
    crypto_id = "perpetual-protocol"
    return cg.get_price(ids = crypto_id, vs_currencies = "usd",
        include_market_cap = "false", include_24hr_change="false")

# Can search any of these queries using https://dune.xyz/queries/{ID}

def get_generated_id_staked_perp_holdings(headers, url):
    from requests import post
    data = """{"operationName":"GetResult","variables":{"query_id":112886},"query":"query GetResult($query_id: Int!, $parameters: [Parameter!]) {\n  get_result(query_id: $query_id, parameters: $parameters) {\n    job_id\n    result_id\n    __typename\n  }\n}\n"}"""
    resp = post(url = url, headers = headers, data=data).json()["data"]["get_result"]["result_id"]
    return resp

def get_staked_perp_holdings(headers, url):
    from requests import post
    generated_id = get_generated_id_staked_perp_holdings(headers, url)
    data = """{"operationName":"FindResultDataByResult","variables":{"result_id":\"""" + generated_id + """\"},"query":"query FindResultDataByResult($result_id: uuid!) {\n  query_results(where: {id: {_eq: $result_id}}) {\n    id\n    job_id\n    error\n    runtime\n    generated_at\n    columns\n    __typename\n  }\n  get_result_by_result_id(args: {want_result_id: $result_id}) {\n    data\n    __typename\n  }\n}\n"}"""
    resp = post(url = url, headers = headers, data = data).json()["data"]["get_result_by_result_id"]
    return resp

def get_generated_id_perp_holdings(headers, url):
    from requests import post
    data = """{"operationName":"GetResult","variables":{"query_id":112895},"query":"query GetResult($query_id: Int!, $parameters: [Parameter!]) {\n  get_result(query_id: $query_id, parameters: $parameters) {\n    job_id\n    result_id\n    __typename\n  }\n}\n"}"""
    resp = post(url = url, headers = headers, data=data).json()["data"]["get_result"]["result_id"]
    return resp

def get_perp_holdings(headers, url):
    from requests import post
    generated_id = get_generated_id_perp_holdings(headers, url)
    data = """{"operationName":"FindResultDataByResult","variables":{"result_id":\"""" + generated_id + """\"},"query":"query FindResultDataByResult($result_id: uuid!) {\n  query_results(where: {id: {_eq: $result_id}}) {\n    id\n    job_id\n    error\n    runtime\n    generated_at\n    columns\n    __typename\n  }\n  get_result_by_result_id(args: {want_result_id: $result_id}) {\n    data\n    __typename\n  }\n}\n"}"""
    resp = post(url = url, headers = headers, data = data).json()["data"]["get_result_by_result_id"]
    return resp

def get_generated_id_funding(headers, url):
    from requests import post
    data = """{"operationName":"GetResult","variables":{"query_id":112823},"query":"query GetResult($query_id: Int!, $parameters: [Parameter!]) {\n  get_result(query_id: $query_id, parameters: $parameters) {\n    job_id\n    result_id\n    __typename\n  }\n}\n"}"""
    resp = post(url = url, headers = headers, data=data).json()["data"]["get_result"]["result_id"]
    return resp

def get_aggregated_funding(headers, url):
    from requests import post
    generated_id = get_generated_id_funding(headers, url)
    data = """{"operationName":"FindResultDataByResult","variables":{"result_id":\"""" + generated_id + """\"},"query":"query FindResultDataByResult($result_id: uuid!) {\n  query_results(where: {id: {_eq: $result_id}}) {\n    id\n    job_id\n    error\n    runtime\n    generated_at\n    columns\n    __typename\n  }\n  get_result_by_result_id(args: {want_result_id: $result_id}) {\n    data\n    __typename\n  }\n}\n"}"""
    resp = post(url = url, headers = headers, data = data).json()["data"]["get_result_by_result_id"]
    return resp

def get_generated_id_by_symbol_liquidations(headers, url):
    from requests import post
    data = """{"operationName":"GetResult","variables":{"query_id":111039},"query":"query GetResult($query_id: Int!, $parameters: [Parameter!]) {\n  get_result(query_id: $query_id, parameters: $parameters) {\n    job_id\n    result_id\n    __typename\n  }\n}\n"}"""
    resp = post(url = url, headers = headers, data = data).json()["data"]["get_result"]["result_id"]
    return resp

def get_liquidations_by_symbol_data(headers, url):
    from requests import post
    generated_id = get_generated_id_by_symbol_liquidations(headers, url)
    data = """{"operationName":"FindResultDataByResult","variables":{"result_id":\"""" + generated_id + """\"},"query":"query FindResultDataByResult($result_id: uuid!) {\n  query_results(where: {id: {_eq: $result_id}}) {\n    id\n    job_id\n    error\n    runtime\n    generated_at\n    columns\n    __typename\n  }\n  get_result_by_result_id(args: {want_result_id: $result_id}) {\n    data\n    __typename\n  }\n}\n"}"""
    resp = post(url = url, headers = headers, data = data).json()["data"]["get_result_by_result_id"]
    return resp

def get_generated_id_liquidations(headers, url):
    from requests import post
    data = """{"operationName":"GetResult","variables":{"query_id":109215},"query":"query GetResult($query_id: Int!, $parameters: [Parameter!]) {\n  get_result(query_id: $query_id, parameters: $parameters) {\n    job_id\n    result_id\n    __typename\n  }\n}\n"}"""
    resp = post(url = url, headers = headers, data = data).json()["data"]["get_result"]["result_id"]
    return resp

def get_liquidations_data(headers, url):
    from requests import post
    generated_id = get_generated_id_liquidations(headers, url)
    data = """{"operationName":"FindResultDataByResult","variables":{"result_id":\""""+ generated_id + """\"},"query":"query FindResultDataByResult($result_id: uuid!) {\n  query_results(where: {id: {_eq: $result_id}}) {\n    id\n    job_id\n    error\n    runtime\n    generated_at\n    columns\n    __typename\n  }\n  get_result_by_result_id(args: {want_result_id: $result_id}) {\n    data\n    __typename\n  }\n}\n"}"""
    resp = post(url = url, headers = headers, data = data).json()["data"]["get_result_by_result_id"]
    return resp

def get_generated_market_bias_id(headers, url):
    from requests import post
    data = """{"operationName":"GetResult","variables":{"query_id":109033},"query":"query GetResult($query_id: Int!, $parameters: [Parameter!]) {\n  get_result(query_id: $query_id, parameters: $parameters) {\n    job_id\n    result_id\n    __typename\n  }\n}\n"}"""
    resp = post(url = url, headers = headers, data = data).json()["data"]["get_result"]["result_id"]
    return resp

def get_market_bias_data(headers, url):
    from requests import post
    generated_id = get_generated_market_bias_id(headers, url)
    data = """{"operationName":"FindResultDataByResult","variables":{"result_id":\"""" + generated_id + """\"},"query":"query FindResultDataByResult($result_id: uuid!) {\n  query_results(where: {id: {_eq: $result_id}}) {\n    id\n    job_id\n    error\n    runtime\n    generated_at\n    columns\n    __typename\n  }\n  get_result_by_result_id(args: {want_result_id: $result_id}) {\n    data\n    __typename\n  }\n}\n"}"""
    resp = post(url = url, headers = headers, data = data).json()["data"]["get_result_by_result_id"]
    return resp

def get_last_transactions():
    from requests import get
    url = "https://ethplorer.io/service/service.php?refresh=transfers&data=0xbc396689893d065f41bc2c6ecbee5e0085233447&page=pageSize=25&showTx=all"
    headers = {
        "Host": "ethplorer.io",
        "Referer": "https://ethplorer.io/address/0xbc396689893d065f41bc2c6ecbee5e0085233447",
        "Accept": "*/*"
    }
    resp_json = get(url = url, headers = headers).json()
    return resp_json["transfers"]

async def retrieve_last_trades_data():
    from aiohttp import ClientSession
    async with ClientSession() as session:
        headers = {
            "Origin": "https://dex.guru",
            "Referer": "https://dex.guru/",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0"
        }
        url = "https://api.dex.guru/v1/tokens/0xbc396689893d065f41bc2c6ecbee5e0085233447-eth/swaps?from_num=0&size=30&sort_by=timestamp&sort_by2=id&asc=false"
        async with session.get(url = url, headers = headers) as resp:
            return await resp.json()

async def update_csrf_token(session):
    url = "https://app.redash.io/static/images/redash_icon_small.png"
    headers = {
        "Accept": "*/*",
        "Pragma": "no-cache",
        "Host": "app.redash.io",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0"
    }
    async with session.get(url = url, headers = headers) as resp:
        return session

async def retrieve_perp_dashboard():
    from aiohttp import ClientSession
    async with ClientSession() as session:
        headers = {
            "Accept": "*/*",
            "Pragma": "no-cache",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0"
        }
        url = "https://app.redash.io/perp/public/dashboards/rpCOTkyNbXOUsanfyi4HvcT5wgougGGhCMPa7GKS?p_WITHIN_DAYS=28&refresh=60"
        async with session.get(url = url, headers = headers) as resp:
            resp = await update_csrf_token(session)
            return await retrieve_funding(resp)

async def retrieve_funding():
    from aiohttp import ClientSession
    async with ClientSession() as session:
        # UPDATE FUNDING FOR CHARTS HERE

        headers = {
            "Host": "app.redash.io",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0",
            "Accept": "application/json, text/plain, */*",
            "Accept-Language": "en-US,en;q=0.5",
            "Accept-Encoding": "gzip, deflate, br",
            "Authorization": "Key RfFGcsZN68RAGKKouqvTxHMJeQQh7J7hfo8LADqR",
            "X-CSRF-TOKEN": "",
            "DNT": "1",
            "Connection": "keep-alive",
        }
        url = "https://app.redash.io/perp/api/queries/717699"

        async with session.get(url = url, headers =headers) as resp:
            result = await resp.json()
        query_id=result["latest_query_data_id"]

        url = f"https://app.redash.io/perp/api/queries/717699/results/{query_id}.json"


        async with session.get(url = url, headers = headers) as resp:
            return await resp.json()

def retrieve_chunk_url(session, token_name):
    url = f"https://perp.exchange/t/{token_name}:USDC"
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko",
        "Accept": "*/*",
        "Pragma": "no-cache",
        "Content-Type": "application/x-www-form-urlencoded"
    }
    resp = session.get(url = url, headers=headers)
    resp = resp.text
    resp = resp.split(""".chunk.js"></script><script src=\"""")[1].rsplit("""\"></script></body></html>""")[0]
    return "https://perp.exchange" + resp


def retrieve_api_key(chunk_url, session):
    url = chunk_url
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko",
        "Accept": "*/*",
        "Pragma": "no-cache",
        "Content-Type": "application/x-www-form-urlencoded"
    }
    resp = session.get(url = url, headers=headers)
    resp = resp.text
    resp = resp.split("""graphql",candleApiKey:\"""")[1].rsplit("""\",candleApiRegion""")[0]
    return resp


def retrieve_mark_price(api_key, session, token_pair):
    url = "https://jp7ymdkf7jcz5hni62kofoxapm.appsync-api.ap-southeast-1.amazonaws.com/graphql"
    data = """{"operationName":"GetStatistics","variables":{"ammAddress":\"""" + token_pair + """\"},"query":"query GetStatistics($ammAddress: String!) {getStatistics(ammAddr: $ammAddress) {  ammAddr  lastTradePriceUsd  volume24h  priceChangeRate24h  priceHigh24h  priceLow24h  timestamp  blockNumber  __typename}}"}"""
    headers = {
        "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate, br",
        "content-type": "application/json",
        "Host": "jp7ymdkf7jcz5hni62kofoxapm.appsync-api.ap-southeast-1.amazonaws.com",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0",
        "x-amz-user-agent": "aws-amplify/2.0.3",
        "X-Api-Key" :api_key
    }
    resp = session.post(url = url, headers = headers, data = data).json()
    return resp

async def retrieve_candlestick_data(api_key, session, token_pair):
    url = "https://jp7ymdkf7jcz5hni62kofoxapm.appsync-api.ap-southeast-1.amazonaws.com/graphql"
    data = """{"operationName":"ListCandleStick","variables":{"query":{"marketResolution":{"eq":\"""" + token_pair + """#1h"}},"limit":9999},"query":"query ListCandleStick($query: TableCandleStickQueryInput!, $limit: Int) {listCandleSticks(query: $query, limit: $limit) {  items {    market    resolution    startTime    open    high    low    close    volume    baseAssetVol    txCount    blockNumber    version    __typename  }  __typename}}"}"""
    headers = {
        "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate, br",
        "content-type": "application/json",
        "Host": "jp7ymdkf7jcz5hni62kofoxapm.appsync-api.ap-southeast-1.amazonaws.com",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0",
        "x-amz-user-agent": "aws-amplify/2.0.3",
        "X-Api-Key" :api_key
    }
    resp = session.post(url = url, headers = headers, data = data).json()
    return resp

def retrieve_funding_payments(amm_info, cg):
    from aiohttp import ClientSession
    from requests import Session
    from commands.pricing_data.crypto import get_crypto_price_data
    session = Session()
    chunk_url = retrieve_chunk_url(session, amm_info["id"].upper())
    api_key = retrieve_api_key(chunk_url, session)
    mark_price = retrieve_mark_price(api_key, session, amm_info["address"])
    mark_price = mark_price["data"]["getStatistics"]
    index_price = get_crypto_price_data(cg, amm_info["symbol"], "usd", "true",
        "false")[amm_info["symbol"]]
    return {"mark_price": mark_price, "index_price": index_price}

def get_amm_funding(symbol_id):
    from subprocess import Popen, PIPE, TimeoutExpired
    from re import split
    if symbol_id:
        process = Popen(f"perp amm {symbol_id}", stdout=PIPE, shell=True)
        result = process.communicate()[0]
        result = result.decode()[:-4].split("\n- ")
        process.stdout.close()

    else:
        process = Popen("perp amm", stdout=PIPE, shell=True)
        # Had issue without timeout check.
        try:
            result = process.communicate(timeout=60)[0]
        except TimeoutExpired:
            process.kill()
            return (None, None)
        result = result.decode()[:-4]
        result_list = split("%\n\n|\n- ", result)
        symbol_lists = result_list[0::11]
        funding_rate_lists = result_list[10::11]
        process.stdout.close()
        return (symbol_lists, funding_rate_lists)

    return result

async def get_open_interest():
    from aiohttp import ClientSession
    async with ClientSession() as session:
        url = "https://app.redash.io/perp/api/queries/572602/results"
        headers = {
            "Accept": "*/*",
            "Authorization": "Key rpCOTkyNbXOUsanfyi4HvcT5wgougGGhCMPa7GKS",
            "Pragma": "no-cache",
            "Host": "app.redash.io",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0",
            "X-CSRF-TOKEN": ""
        }
        data = """{"id":572602,"parameters":{}}"""
        async with session.post(url = url, headers = headers, data=data) as resp:
            return await resp.json()

async def retrieve_profile(address):
    from gql import gql, Client
    from gql.transport.requests import RequestsHTTPTransport
    from gql.transport.aiohttp import AIOHTTPTransport
    transport = AIOHTTPTransport(url = "https://api.thegraph.com/subgraphs/name/vipineth/perpetual-protocol-stats")

    async with Client(
        transport=transport, fetch_schema_from_transport=True, execute_timeout=60
    ) as client:
        query = gql(
            """{
                user(id: \"""" + address + """\") {
                    totalPnL,
                    totalTrades,
                    totalVolume,
                    address,
                    transactions(first: 1, orderBy: totalPnlAmount, orderDirection: desc) {
                        amm,
                        date,
                        unrealizedPnlAfter,
                        transactionHash,
                        totalPnlAmount,
                        spotPrice,
                        realizedPnl,
                        positionSizeAfter,
                        positionNotional,
                        margin,
                        fundingPayment,
                        fee,
                        exchangedPositionSize
                    }
                }
            }"""
        )
        result = await client.execute(query)
        return result

async def retrieve_staked_url():
    url = "https://s3.amazonaws.com/staking.perp.fi/production/snapshot-immediate.json"
    from aiohttp import ClientSession
    async with ClientSession() as client:
       async with client.get(url = url) as resp:
           return await resp.json()

async def retrieve_staked_vesting_url():
    url =  "https://s3.amazonaws.com/staking.perp.fi/production/snapshot-vesting.json"
    from aiohttp import ClientSession
    async with ClientSession() as client:
       async with client.get(url = url) as resp:
           return await resp.json()

def retrieve_staking_data(urls, dates_data, is_vested):
    from concurrent.futures import as_completed
    from requests_futures.sessions import FuturesSession
    # FutureSession runs requests in parallel, faster approach to receiving responses.
    # https://julien.danjou.info/python-and-fast-http-clients/
    staked_data = []
    url_subsection_len = len("https://s3.amazonaws.com/staking.perp.fi/production/")
    with FuturesSession(max_workers = 10) as session:
        futures = [session.get(url) for url in urls]
        for future in as_completed(futures):
            resp = future.result()
            week = resp.request.url[url_subsection_len:]
            week = week[:week.find("/")]
            result = {
                "week": week,
                "resp": resp.json(),
            }
            if is_vested:
                result["unlock_date"] = dates_data[week]["redeemableUntil"]
            staked_data.append(result)
    # Since the data is retrieved in an unordered list, we want to sort it.
    staked_data.sort(key = lambda key: int(key["week"]))
    return staked_data

def retrieve_leaderboard_data():
    from gql import gql, Client
    from gql.transport.requests import RequestsHTTPTransport
    # Had issues with AIOHTTPTransport, adding it for later if needed.

    # https://github.com/graphql-python/gql/issues/203
    # from gql.transport.aiohttp import AIOHTTPTransport
    # transport = AIOHTTPTransport(
    #     url = "https://api.thegraph.com/subgraphs/name/perpetual-protocol/perp-position-subgraph"
    # )
        # async with Client(
    #     transport=transport, fetch_schema_from_transport=True, execute_timeout=75
    # ) as client:
    #     query = gql("""{
    #             positions(first: 5, orderBy: totalPnlAmount, orderDirection: desc) {
    #                 id
    #                 trader
    #                 fee
    #                 totalPnlAmount
    #             }
    #         }
    #         """
    #                 # openNotional
    #     )
    #     result = await client.execute(query)

    transport = RequestsHTTPTransport(
        url = "https://api.thegraph.com/subgraphs/name/perpetual-protocol/perp-position-subgraph",
        retries = 3,
        verify = True
    )
    client = Client(transport = transport, fetch_schema_from_transport=True)
    query = gql("""{
        positions(first: 5, orderBy: totalPnlAmount, orderDirection: desc) {
            id
            trader
            fee
            totalPnlAmount
            }
        }"""
    )

    result = client.execute(query)


    return result

# async def retrieve_funding_data():
#     from gql import gql, Client
#     from gql.transport.aiohttp import AIOHTTPTransport
#     from time import time
#     transport = AIOHTTPTransport(url = "https://api.thegraph.com/subgraphs/name/perpetual-protocol/perp-position-subgraph")
    # https://github.com/graphql-python/gql/issues/203
#     async with Client(
#         transport=transport, fetch_schema_from_transport=True
#     ) as client:
#         query = gql(
#             """{
#             fundingRateUpdatedEvents(where: {
#                 timestamp_lt :"1629806460"
#             }, orderBy: blockNumber, orderDirection: asc) {
#                     id
#                     amm
#                     rate
#                     underlyingPrice
#                     timestamp
#                     blockNumber
#                 }
#             }"""
#         )
#         result = await client.execute(query)
#         return result

