from discord import Embed
# def create_market_cap_embed():

#     from commands.perpetual.helper import random_hex_colour
#     from .perp_data import get_circulating_supply, get_price
#     circulating_supply = float(get_circulating_supply())
#     price = get_price()["perpetual-protocol"]["usd"]
#     market_cap_embed = Embed(title = f"Current Market Cap: ${circulating_supply * price:,.2f}")
#     market_cap_embed.set_footer(text = "Perpetual Protocol Community Bot",
#     icon_url="https://s2.coinmarketcap.com/static/img/coins/128x128/6950.png")
#     return market_cap_embed

def format_value(price):
    is_small = True if int(price) < 1 else False
    if is_small:
        return ("%0.6f" % price).rstrip("0")
    return f"{price:,.2f}"

def get_addr_resp(token_addr):
    if token_addr == "0x3f5ce5fbfe3e9af3971dd833d26ba9b5c936f0be":
        res = "[Binance]"
    elif token_addr == "0xf54025af2dc86809be1153c1f20d77adb7e8ecf4":
        res = "[Balancer: PERP/USDC]"
    elif token_addr == "0x8486c538dcbd6a707c5b3f730b6413286fe8c854":
        res = "[Sushiswap: PERP]"
    elif token_addr == "0xdb38ae75c5f44276803345f7f02e95a0aeef5944":
        res = "[1inch v3: Router]"
    elif token_addr == "0xf66369997ae562bc9eec2ab9541581252f9ca383":
        res = "[Uniswap V2: PERP]"
    elif token_addr == "0x6cc5f688a315f3dc28a7781717a9a798a59fda7b":
        res = "[OKEx]"
    else:
        res = "[User]"
    res += f"(https://etherscan.io/address/{token_addr})"
    return res

def get_last_transactions():
    from .perp_data import get_last_transactions, get_price
    from datetime import datetime
    current_time = datetime.now()
    last_txs = get_last_transactions()
    last_txs = [last_txs[:4], last_txs[4:8], last_txs[8:12], last_txs[12:16]]
    price = get_price()
    return create_last_transactions(last_txs, price)

async def get_perp_trades():
    from .perp_data import retrieve_last_trades_data
    last_trades = await retrieve_last_trades_data()
    last_trades = last_trades["data"]
    last_trades = [last_trades[:4], last_trades[4:8], last_trades[8:12], last_trades[12:16]]
    return create_last_trades_embed(last_trades)

async def get_perp_open_interest():
    from .perp_data import get_open_interest
    from .perp_helper import format_open_interest
    data = await get_open_interest()
    data_set = data["query_result"]["data"]["rows"]
    data_set = format_open_interest(data_set)
    return create_open_interest_collect_embed(data_set)

def get_funding(funding_rate_lists, symbol_lists, token_name):
    from .perp_helper import lookup_symbol
    arguments = lookup_symbol(token_name.upper())
    if not arguments:

        result = None
        # Error Embed (have been suggested to remove these)
        #error_message = f"Unfortunately I could not find {token_name} on the Perpetual Exchange! Please enter a valid symbol."
        #create_invalid_embed(error_message)
    else:
        trading_pair = f"{arguments['id'].upper()}/USDC"
        funding_rate_index = symbol_lists.index(trading_pair)
        funding_rate = funding_rate_lists[funding_rate_index]
        result = amm_funding(arguments, funding_rate, trading_pair)
    return result

def get_liquidations(liquidation_by_symbol_data, symbol):
    from .perp_helper import lookup_symbol, format_liquidations_by_symbol
    arguments = lookup_symbol(symbol.upper())
    if not arguments:
        result = None
    else:
        trading_pair = f"{arguments['id'].upper()}/USDC"
        formatted_data_set = format_liquidations_by_symbol(liquidation_by_symbol_data, arguments["id"].upper())
        result = create_liquidations_symbol_embed(arguments, formatted_data_set, trading_pair)
    return result

def create_last_transactions(last_txs, price):
    from datetime import datetime
    from commands.perpetual.helper import random_hex_colour
    from .perp_helper import add_wallet_type, process_time, splice_time
    current_time = datetime.now()
    tx_collection = []
    page_no = 1
    tx_count = 1
    icon_url="https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png"
    for transactions in last_txs:
        transaction_embed = Embed(title = f"PERP's Last Transactions (Page {page_no})",
        colour = random_hex_colour())
        transaction_embed.set_author(name = "Perpetual Protocol", url = "https://perp.exchange/",
        icon_url=icon_url)
        transaction_embed.set_thumbnail(url=icon_url)
        for tx in transactions:
            # Multiplication operation faster than division
            num_tokens = int(tx["value"]) * 0.000000000000000001
            token_price = tx["usdPrice"] if "usdPrice" in tx.keys() else price["perpetual-protocol"]["usd"]
            tx_value = num_tokens * token_price
            num_tokens = format_value(num_tokens)
            tx_value_desc = format_value(tx_value)
            from_addr = get_addr_resp(tx["from"])
            to_addr = get_addr_resp(tx["to"])

            tx_desc = f"**From:** {from_addr}\n"
            tx_desc += f"**To:** {to_addr}\n"
            tx_desc += f"**Value:** ${tx_value_desc} USD ({num_tokens} PERP) "
            tx_desc += add_wallet_type(tx_value)
            tx_desc += splice_time(current_time, tx["timestamp"])

            transaction_embed.add_field(name = f"Transaction {tx_count}",
            value = tx_desc, inline=False)
            tx_count += 1
        page_no += 1
        transaction_embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url = icon_url)
        tx_collection.append(transaction_embed)
    return tx_collection

def create_last_trades_embed(last_trades):
    from datetime import datetime
    from commands.perpetual.helper import random_hex_colour
    from .perp_helper import add_wallet_type, get_transaction_url
    current_time = datetime.now()
    trades_collection = []
    page_no = 1
    trade_count = 1
    icon_url="https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png"
    for trade in last_trades:
        last_trades_embed = Embed(title = f"Last Trades (Page {page_no})",
            colour = random_hex_colour())
        last_trades_embed.set_author(name = "Perpetual Protocol", url = "https://perp.exchange/",
        icon_url=icon_url)
        last_trades_embed.set_thumbnail(url=icon_url)
        for data_point in trade:
            data_point["timestamp"]
            amount_eth_out = data_point["amount0Out"]
            amount_perp_in = data_point["amount1In"]
            trade_type = "Buy" if  amount_eth_out and amount_perp_in else "Sell"
            desc = f"**Trade type:** {trade_type}\n"
            desc += f"**Value:** ${data_point['amountUSD']:,.2f} USD "
            perp_amount = data_point['amount0In'] if trade_type == "Sell" else amount_eth_out
            desc += f"({perp_amount:,.2f} PERP) "
            desc += add_wallet_type(data_point['amountUSD'])
            exchange_type = data_point["AMM"]
            if exchange_type == "uniswap":
                desc += f"**Exchange:** <:uniswap:818748492997459988>\n"
            if exchange_type.lower() == "sushiswap":
                desc += f"**Exchange:** <:sushiswap:818748553374859294>\n"
            tx_url = get_transaction_url(data_point["transactionAddress"])
            desc += f"[Transaction]({tx_url})\n"
            last_trades_embed.add_field(name = f"Trade {trade_count}",
            value = desc, inline=False)
            trade_count += 1
        page_no += 1
        last_trades_embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url =icon_url)
        trades_collection.append(last_trades_embed)
    return trades_collection

async def create_funding_collection_embed(symbol_lists, funding_rate_lists, query_type):
    from .perp_data import retrieve_perp_dashboard, retrieve_funding, get_amm_funding
    from .perp_helper import format_funding_tokens
    from commands.perpetual.helper import random_hex_colour
    funding_rate_desc = format_funding_tokens(funding_rate_lists, symbol_lists, query_type)
    title = "Current Funding"

    if query_type:
        title += " " + query_type.capitalize()

    if not funding_rate_desc:
        funding_rate_desc += f"Currently there are no AMMs that pay traders for being **{query_type.upper()}**!"

    icon_url="https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png"
    funding_collection_embed = Embed(title=title, description = funding_rate_desc,
        colour = random_hex_colour())
    funding_collection_embed.set_author(name = "Perpetual Protocol", url = "https://perp.exchange/",
        icon_url = icon_url)
    funding_collection_embed.set_thumbnail(url=icon_url)
    funding_collection_embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url =icon_url)
    return funding_collection_embed

def amm_funding(amm_info, amm_funding_rate, trading_pair):
    from commands.pricing_data.crypto import get_crypto_image_data
    from .perp_data import retrieve_funding_payments, get_amm_funding
    from pycoingecko import CoinGeckoAPI
    from .perp_helper import get_image_url_dominant_hex_colours, convert_str_to_hex
    cg = CoinGeckoAPI()
    result = retrieve_funding_payments(amm_info, cg)
    icon_url="https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png"

    amm_funding_info = result["mark_price"]

    funding_rate = amm_funding_rate[len("est.funding rate: "):]
    funding_rate = float(funding_rate)
    apr = funding_rate * 365 * 24
    amm_funding_info['volume24h'] = float(amm_funding_info['volume24h'])
    amm_title = f"Funding Rate: {funding_rate:.4f} % (APR: {apr:.2f} %)\n"
    amm_title += f"Volume (24h): ${amm_funding_info['volume24h']:,.2f}"

    image_url = get_crypto_image_data(cg, amm_info["symbol"], True)
    hex_colour = get_image_url_dominant_hex_colours(image_url, 2)
    trading_url = "https://perp.exchange/t/" + trading_pair.replace("/", ":")
    funding_embed = Embed(title = amm_title, colour = convert_str_to_hex(hex_colour[1][1:]))
    funding_embed.set_author(name = trading_pair, url = trading_url, icon_url=image_url)
    funding_embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url = icon_url)
    return funding_embed

def create_open_interest_collect_embed(data_set):
    from commands.perpetual.helper import random_hex_colour
    open_interest_collection = []
    counter = 0
    page_no = 1
    icon_url="https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png"
    for key, value in data_set.items():
        if counter % 3 == 0:
            interest_embed = Embed(title = f"Current Open Interest (Page {page_no})",
                colour = random_hex_colour())
            interest_embed.set_thumbnail(url = icon_url)
            interest_embed.set_author(name = "Perpetual Protocol", url = "https://perp.fi/",
                icon_url=icon_url)
            interest_embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url = icon_url)
        desc = ""
        title = f"Market Pair: {key}\n"
        for position in value:
            desc += f"**Position:** {position['_side']}\n"
            desc += f"**Open Interest:** ${position['_openInterestUSDC']:.2f}\n\n"
        interest_embed.add_field(name = title, value = desc, inline=False)
        if counter == 2 or counter > 0 and counter % 3 == 0:
            page_no += 1
            open_interest_collection.append(interest_embed)
        counter += 1

    return open_interest_collection

def create_market_bias_url(market_bias_data):
    from quickchart import QuickChart, QuickChartFunction
    qc = QuickChart()
    qc.width = 800
    qc.height = 600
    qc.background_color = "transparent"
    # https://stackoverflow.com/a/45693564
    qc.config = {
        "type": "bar",
        "data": {
            "datasets": [{
                "backgroundColor": ["#3ceaaa" for _ in range(7)],
                "borderColor": ["#04631d" for _ in range(7)],
                "borderWidth": [4 for _ in range(7)],
                "data": market_bias_data
            },
            {
                "backgroundColor": ["#e61947" for _ in range(7)],
                "borderColor": ["#04631d" for _ in range(7)],
                "borderWidth": [4 for _ in range(7)],
                "data": market_bias_data

            }]
        },
        "options": {
            "scales": {
                "xAxes": [{
                    "type": "time",
                    "distribution": "series",
                    "offset": "true",
                    "ticks": {
                        "fontStyle": "bold",
                        "fontSize": "18",
                        "fontColor": "white",
                    }
                }],
                "yAxes": [{
                    "gridLines": {
                        "display": "false"
                    },
                    "ticks": {
                        "callback": QuickChartFunction('(val) => "$" + val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")'),
                        "fontStyle": "bold",
                        "fontSize": "18",
                        "fontColor": "white",

                    }
                }],
            },
            "plugins": {
                "roundedBars": "true"
            },

        }
    }


def get_quickchart_liquidations_collection_url(liquidation_data,
    background_color, border_color, time_period):
    from quickchart import QuickChart, QuickChartFunction

    qc = QuickChart()
    qc.width = 800
    qc.height = 600
    qc.background_color = "transparent"
    # https://stackoverflow.com/a/45693564
    qc.config = {
        "type": "bar",
        "data": {
            "datasets": [{
                "label": "Notional Value",
                # "fill": "false",
                "backgroundColor": [background_color for _ in range(time_period)],
                "borderColor": [border_color for _ in range(time_period)],
                "borderWidth": [4 for _ in range(time_period)],
                "data": liquidation_data
            }]
        },
        "options": {
            "scales": {
                "xAxes": [{
                    "type": "time",
                    "distribution": "series",
                    "offset": "true",
                    "ticks": {
                        "fontStyle": "bold",
                        "fontSize": "18",
                        "fontColor": "white",
                    }
                }],
                "yAxes": [{
                    "gridLines": {
                        "display": "false"
                    },
                    "ticks": {
                        "callback": QuickChartFunction('(val) => "$" + val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")'),
                        "fontStyle": "bold",
                        "fontSize": "18",
                        "fontColor": "white",

                    }
                }],
            },
            "plugins": {
                "roundedBars": "true"
            },

        }
    }
    return qc.get_url()


def create_liquidations_symbol_embed(amm_info, liquidations_data, trading_pair):
    from commands.pricing_data.crypto import get_crypto_image_data
    from .perp_helper import get_image_url_dominant_hex_colours, convert_str_to_hex
    from pycoingecko import CoinGeckoAPI
    icon_url="https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png"
    title = f"Liquidations for {trading_pair}"
    cg = CoinGeckoAPI()
    image_url = get_crypto_image_data(cg, amm_info["symbol"], True)

    hex_colours = get_image_url_dominant_hex_colours(image_url, 2)
    background_colour = hex_colours[1]
    border_colour = hex_colours[0]

    trading_url = "https://perp.exchange/t/" + trading_pair.replace("/", ":")
    embed = Embed(title = title, colour = convert_str_to_hex(background_colour[1:]))
    embed.set_author(name = trading_pair, url = trading_url, icon_url=image_url)
    embed.set_image(url = get_quickchart_liquidations_collection_url(liquidations_data,
        background_colour, border_colour, 8))
    embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url = icon_url)
    return embed

def create_liquidations_embed(liquidation_data):
    from .perp_helper import convert_str_to_hex
    icon_url="https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png"
    title = "Liquidations for all symbols"
    background_colour = "#3ceaaa"
    border_colour = "#04631d"
    embed = Embed(title = title, colour = convert_str_to_hex(background_colour[1:]))
    embed.set_author(name = "Perpetual Protocol", url = "https://perp.exchange/",
        icon_url = icon_url)
    embed.set_image(url = get_quickchart_liquidations_collection_url(liquidation_data,
    background_colour, border_colour, 7))

    embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url =icon_url)
    return embed

async def get_profile_address(address, aggregated_funding, perp_holdings,
    sperp_holdings):
    from .perp_data import retrieve_profile
    from .perp_helper import get_stripped_address, \
        lookup_address_in_dune_dataset

    address = get_stripped_address(address)
    if not address.startswith("0x"):
        return False

    is_user = await retrieve_profile(address)
    is_user = is_user["user"]

    if not is_user:
        return False

    is_aggregated_funding = lookup_address_in_dune_dataset(address,
        aggregated_funding, "trader")
    is_perp_holder = lookup_address_in_dune_dataset(address, perp_holdings, "address")
    is_sperp_holder = lookup_address_in_dune_dataset(address, sperp_holdings, "address")
    return create_profile_embed(user_data=is_user,
        aggregated_funding=is_aggregated_funding, perp_holder = is_perp_holder,
        sperp_holder = is_sperp_holder)

def get_staking_vesting_dates(address, staking_vested_data,
    staked_vested_dates_data):
    from .perp_helper import get_stripped_address, get_total_accrued_rewards, \
    lookup_address_in_dune_dataset, get_next_vested_unlock
    address = get_stripped_address(address)
    if not address.startswith("0x"):
        return False

    next_unlocks = get_next_vested_unlock(address, staked_vested_dates_data,
        staking_vested_data, 5)

    if next_unlocks == []:
        return False

    return create_staking_vesting_embed(address, next_unlocks)

def get_staking_address(address, staking_vested_data, staked_vested_dates_data, \
    staking_immediate_data, sperp_holdings):
    from .perp_helper import get_stripped_address, get_total_accrued_rewards, \
    lookup_address_in_dune_dataset, get_next_vested_unlock
    address = get_stripped_address(address)
    if not address.startswith("0x"):
        return False

    immediate_rewards = get_total_accrued_rewards(address, staking_immediate_data)
    vesting_rewards = get_total_accrued_rewards(address, staking_vested_data)

    # If no addresses with rewards, means the address has no staking information.
    if immediate_rewards == 0 and vesting_rewards == 0:
        return False

    total_rewards = {"immediate": immediate_rewards, "vested": vesting_rewards}
    is_sperp_holder = lookup_address_in_dune_dataset(address, sperp_holdings, "address")

    next_unlock = get_next_vested_unlock(address, staked_vested_dates_data,
        staking_vested_data, 1)
    return create_staking_embed(address, next_unlock, total_rewards, is_sperp_holder)


def create_profile_embed(user_data, aggregated_funding, perp_holder, sperp_holder):

    # PROFILE REGISTER
    # if register you confirm your avatar will be showcased in the Perpetual Protocol bot.
    # You can remove your profile anytime. By saying Profile Remove

    from .perp_helper import get_account_url, convert_str_to_hex, \
        lookup_amm_address, format_the_graph_number, add_wallet_type
    from commands.perpetual.charts.chart_helper import human_decimal_format
    from datetime import datetime

    icon_url="https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png"
    title = "Trader Profile"
    address = user_data['address']
    etherscan_link = get_account_url(address)

    description = f"[{address}]({etherscan_link})"

    colour = convert_str_to_hex("3ceaaa")

    embed = Embed(title=title, description=description, color=colour)

    total_pnl = format_the_graph_number(float(user_data["totalPnL"]))
    total_pnl = "$" + human_decimal_format(total_pnl)

    total_volume =  format_the_graph_number(float(user_data["totalVolume"]))
    total_volume = "$" + human_decimal_format(total_volume)

    total_trades = int(user_data["totalTrades"])
    if total_trades * 0.001 >= 1:
        total_trades = human_decimal_format(total_trades)

    best_trade = user_data["transactions"][0]
    best_trade_date = datetime.fromtimestamp(float(best_trade["date"]))
    # https://stackoverflow.com/a/1759498
    best_trade_date = best_trade_date.strftime("%d/%m/%y %H:%M %p")
    best_trade_pnl = float(best_trade["totalPnlAmount"])
    best_trade_pnl = format_the_graph_number(best_trade_pnl)
    best_trade_wallet = add_wallet_type(best_trade_pnl)
    best_trade_pnl = "$" + human_decimal_format(best_trade_pnl)

    best_trade_side = float(best_trade['exchangedPositionSize'])
    best_trade_side = "SHORT" if best_trade_side < 0 else "LONG"
    best_trade_amm = lookup_amm_address(best_trade["amm"])
    best_trade_desc = f"**Date:** {best_trade_date}\n"
    best_trade_desc += f"**Side:** {best_trade_side}\n"
    best_trade_desc += f"**AMM**: {best_trade_amm}\n"
    best_trade_desc += f"**PNL:** {best_trade_pnl} {best_trade_wallet}\n"

    embed.add_field(name = "Total PNL", value = total_pnl)

    embed.add_field(name = "Total Volume", value = total_volume)

    embed.add_field(name = "Total Trades", value = total_trades)

    if aggregated_funding and float(aggregated_funding["totalFundingPayment"]) != 0:
        total_funding_payment = float(aggregated_funding["totalFundingPayment"])
        name = "Total Funding "
        if total_funding_payment > 0:
            name += "Earned"
        else:
            name += "Paid"
        total_funding_payment = format_the_graph_number(total_funding_payment)
        total_funding_payment = "$" + human_decimal_format(total_funding_payment)
        embed.add_field(name = name, value = total_funding_payment)

    name = "Most Traded AMM"
    most_traded_amm_frequency = int(aggregated_funding["frequency"])
    is_large = "times" if most_traded_amm_frequency > 1 else "time"
    most_traded_amm = "{} ({:,} {})".format(aggregated_funding["mostTradedAMM"],
        (most_traded_amm_frequency), is_large)
    embed.add_field(name = name, value = most_traded_amm)

    embed.add_field(name = "Best Trade", value = best_trade_desc, inline=False)

    if sperp_holder or perp_holder:
        name = "Holdings"
        value = ""
        if perp_holder:
            value += f"**SPERP:** {format_value(perp_holder['holdings'])}\n"
        if sperp_holder:
            value += f"**PERP:** {format_value(sperp_holder['holdings'])}\n"
        embed.add_field(name = name, value = value)

    embed.set_thumbnail(url = "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png")
    embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url =icon_url)
    return embed

def create_leaderboard_embed(leaderboard_data):
    from .perp_helper  import get_account_url, convert_str_to_hex, \
        format_the_graph_number, add_wallet_type, get_trader_url, get_account_url
    from commands.perpetual.charts.chart_helper import human_decimal_format
    from datetime import datetime
    icon_url="https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png"

    title = f"Top Traders Leaderboard by PNL"
    colour = convert_str_to_hex("3ceaaa")
    embed = Embed(title = title, colour = colour)

    positions = leaderboard_data["positions"]
    counter = 1

    for position in positions:
        # Had issues with Perp subgraph for the trading time (timeout).
        # trading_time = position["ammPositions"][0]["timestamp"]
        # trading_time = datetime.fromtimestamp(int(trading_time))
        # trading_time = trading_time.strftime("%d/%m/%y")

        field_name = f"Ranked #{counter}"
        address = position["trader"]

        address_link = get_account_url(address)
        trader_link = get_trader_url(address)
        pnl = format_the_graph_number(float(position["totalPnlAmount"]))
        pnl = "$" + format_value(pnl)
        fees = format_the_graph_number(float(position["fee"]))
        fees = "$" + format_value(fees)
        desc = f"**PNL:** {pnl}\n"
        desc += f"**Total Fees:** {fees}\n"
        # desc += f"**Trading Since:** {trading_time}\n"
        if counter > 1:
            embed.add_field(name = "\u200B", value = "\u200B", inline=False)
        embed.add_field(name = field_name, value = desc, inline=True)
        field_name = "Links"
        desc = f"**[PerpTerminal]({trader_link})**\n"
        desc += f"**[BlockScout]({address_link})**\n"
        embed.add_field(name = field_name, value = desc, inline=True)
        counter += 1
    embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url =icon_url)

    return embed

def create_staking_embed(address, next_unlock, total_rewards, sperp_holder):
    from .perp_helper  import get_account_url, convert_str_to_hex, \
        format_the_graph_number, get_trader_url, get_account_url
    from commands.perpetual.charts.chart_helper import human_decimal_format
    from .perp_data import get_price
    from datetime import datetime
    icon_url="https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png"

    title = "Staking Account"
    desc = "**[{}]({})**\n".format("BlockScout", get_account_url(address))
    colour = convert_str_to_hex("3ceaaa")

    embed = Embed(title = title, description = desc, colour = colour)
    price = get_price()["perpetual-protocol"]["usd"]

    staking_desc = ""
    if sperp_holder:
        staking_desc += f"**Staked PERP:** {sperp_holder['holdings']} SPerp\n"

    vested_rewards = total_rewards["vested"]
    vested_rewards_value = vested_rewards * price
    vested_rewards_value = human_decimal_format(vested_rewards_value)
    vested_rewards = format_value(vested_rewards)

    sum_rewards = total_rewards["immediate"] + total_rewards["vested"]
    sum_rewards_value = price * sum_rewards
    sum_rewards_value = human_decimal_format(sum_rewards_value)
    sum_rewards = format_value(sum_rewards)

    staking_desc += f"**Total Rewards:** {sum_rewards} PERP (${sum_rewards_value})\n"
    staking_desc += f"**Vested Rewards:** {vested_rewards} PERP (${vested_rewards_value})\n"
    embed.add_field(name = "Staking", value = staking_desc)

    if next_unlock != []:
        next_unlock = next_unlock[0]
        next_unlock_rewards = float(next_unlock["resp"]["totalRewardPerp"])
        weighting = 100 * next_unlock_rewards / float(next_unlock["total_reward_pool"])
        weighting = format_value(weighting)
        next_unlock_rewards_value = price * next_unlock_rewards
        next_unlock_rewards_value = human_decimal_format(next_unlock_rewards_value)
        next_unlock_rewards = format_value(next_unlock_rewards)
        unlock_date = datetime.fromtimestamp(next_unlock["unlock_date"])
        unlock_date = unlock_date.strftime("%d/%m/%y %H:%M %p")
        unlock_desc = f"**Week {next_unlock['week']} Unlock Date:** {unlock_date}\n"
        unlock_desc += f"**Rewards:** {next_unlock_rewards} PERP (${next_unlock_rewards_value})\n"
        unlock_desc += f"**Weight in Reward Pool:** {weighting}%".format(weighting = weighting)
        embed.add_field(name = "Next Unlock", value = unlock_desc, inline=False)

    embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url =icon_url)
    return embed

def create_staking_vesting_embed(address, next_unlocks):
    from .perp_helper  import get_account_url, convert_str_to_hex, \
        format_the_graph_number, get_trader_url, get_account_url
    from commands.perpetual.charts.chart_helper import human_decimal_format
    from .perp_data import get_price
    from datetime import datetime

    icon_url="https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png"

    title = "Next Staking Vesting Dates"
    desc = "**[BlockScout]({})**\n".format(get_account_url(address))
    desc += "**[PerpTerminal]({})**\n".format(get_trader_url(address))
    colour = convert_str_to_hex("3ceaaa")
    price = get_price()["perpetual-protocol"]["usd"]

    embed = Embed(title = title, description = desc, colour = colour)

    for next_unlock in next_unlocks:
        next_unlock_rewards = float(next_unlock["resp"]["totalRewardPerp"])
        weighting = 100 * next_unlock_rewards / float(next_unlock["total_reward_pool"])
        weighting = format_value(weighting)
        next_unlock_rewards_value = price * next_unlock_rewards
        next_unlock_rewards_value = human_decimal_format(next_unlock_rewards_value)
        next_unlock_rewards = format_value(next_unlock_rewards)
        unlock_date = datetime.fromtimestamp(next_unlock["unlock_date"])
        unlock_date = unlock_date.strftime("%d/%m/%y %H:%M %p")
        unlock_desc = f"**Date:** {unlock_date}\n"
        unlock_desc += f"**Rewards:** {next_unlock_rewards} PERP (${next_unlock_rewards_value})\n"
        unlock_desc += f"**Weight in Reward Pool:** {weighting}%".format(weighting = weighting)
        embed.add_field(name = f"Week {next_unlock['week']} Unlock", value = unlock_desc, inline=False)
    embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url =icon_url)
    return embed

def get_amm_bias(bias_dataset, market, time_period, bias_type):
    from commands.perpetual.charts.chart_helper import format_bias_data, format_bias_type_data
    from .perp_helper import lookup_symbol
    bias_dataset = format_bias_data(bias_dataset, time_period, market)
    amm_bias_dataset = bias_dataset[market.upper()]
    amms, long_bias, short_bias = format_bias_type_data(bias_dataset, bias_type)
    return create_amm_bias_embed(market, time_period, long_bias[0], short_bias[0],
        amm_bias_dataset["LONG"]["position_size"], amm_bias_dataset["SHORT"]["position_size"])

def create_amm_bias_embed(market, time_period, long_bias_percent, short_bias_percent,
    long_bias_value, short_bias_value):
    from .perp_helper import convert_str_to_hex
    icon_url="https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png"
    day_type = "days" if time_period > 1 else "day"
    title = f"Market Bias for {market} in {time_period} {day_type}\n"
    desc = f"**Bullish:** {long_bias_percent:.2f} % (${long_bias_value:,.2f} USD)\n"
    desc += f"**Bearish:** {short_bias_percent:.2f} % (${short_bias_value:,.2f} USD)\n"

    colour = convert_str_to_hex("3ceaaa")
    embed = Embed(title = title, description = desc, colour = colour)
    embed.set_footer(text = "Perpetual Protocol Community Bot", icon_url =icon_url)
    return embed