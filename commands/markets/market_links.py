def create_exchange_db(engine):
    from sqlalchemy import MetaData, Table, Column, String
    metadata = MetaData(engine)
    table = Table("exchanges", metadata,
        Column("exchangeName", String, primary_key = True),
        Column("exchangeLink", String),
        Column("marketPair", String, primary_key = True))
    metadata.create_all()

def check_dupes(engine, exchange_name, market_pair):
    engine.execute(
    """SELECT exchangeName
       FROM exchanges
       WHERE exchangeName == ? and market_pair""", exchange_name,
       market_pair).fetchone() != 0

def get_database_exchanges(is_update):
    from sqlalchemy import create_engine
    from os.path import exists
    new_engine = create_engine("sqlite:///discord.db")
    if not exists("discord.db") or is_update:
        from pandas import DataFrame
        from requests import get
        if not is_update:
            create_exchange_db(new_engine)

        headers = {
            "Host": "web-api.coinmarketcap.com",
            "Origin": "https://coinmarketcap.com",
            "Referer": "https://coinmarketcap.com/"
        }
        url = "https://web-api.coinmarketcap.com/v1/cryptocurrency/market-pairs/latest?slug=perpetual-protocol&start=1&limit=100&convert=USD&category=spot&sort=market_reputation&aux=num_market_pairs,category,market_url,notice,price_quote,effective_liquidity,market_score,market_reputation"
        resp = get(url = url, headers = headers).json()
        market_pairs = resp["data"]["market_pairs"]
        for pair in market_pairs:
            market_dict = {}
            market_dict["exchangeName"] = pair["exchange"]["name"]
            market_dict["exchangeLink"] = pair["market_url"]
            market_dict["marketPair"] = pair["market_pair"]
            if market_dict["exchangeLink"]:
                df = DataFrame.from_dict(market_dict, orient="index").T.set_index("exchangeName")
                df.to_sql("exchanges", con = new_engine, if_exists="append")
    return new_engine

def retrieve_markets():
    from sqlalchemy import create_engine
    new_engine = get_database_exchanges(False)
    return new_engine.execute("SELECT * FROM EXCHANGES ORDER BY exchangeName").fetchall()

def get_emote(exchange_name):
    exchange_name = exchange_name.lower()
    # All emotes stored in my private server.
    if exchange_name == "binance":
        return "<:binance:818748532428242974>"
    elif exchange_name == "uniswap (v2)":
        return "<:uniswap:818748492997459988>"
    elif exchange_name == "hotbit":
        return "<:hotbit:818748447964004353>"
    elif exchange_name == "gate.io":
        return "<:gateio:818748504729976872>"
    elif exchange_name == "hoo":
        return "<:hoo:818748475092762654>"
    elif exchange_name == "okex":
        return "<:okex:818748519568769024>"
    elif exchange_name == "bilaxy":
        return "<:bilaxy:818748461309493298>"
    elif exchange_name == "mxc.com":
        return "<:mxc:818748435015532554>"
    elif exchange_name == "sushiswap":
        return "<:sushiswap:818748553374859294>"
    elif exchange_name == "1inch exchange":
        return "<:1inch:820775010242265098>"
    elif exchange_name == "ftx":
        return "<:ftx:848041669382766602>"
    elif exchange_name == "coinex":
        return "<:coinex:848042153732472863>"
    elif exchange_name == "hitbtc":
        return "<:hitbtc:848042475432181787>"
    elif exchange_name == "balancer":
        return "<:balancer:848042785234747412>"
    elif exchange_name == "bkex":
        return "<:bkex:848043144913092610>"
    elif exchange_name == "bibox":
        return "<:bibox:848043265437204500>"
    return "<:perp:848040000024870923>"

def create_markets_embed():
    from discord import Embed
    from commands.pricing_data.helper import random_hex_colour
    icon_url="https://cdn.discordapp.com/attachments/751595790559871060/834949635843031040/e5cb0e52b49c05d576061ecb89973ed9.png"

    markets = retrieve_markets()

    # https://note.nkmk.me/en/python-multi-variables-values/
    exchange_names = map(lambda x: x[0], markets)
    market_pairs = map(lambda x: x[1:], markets)
    old_name = None
    factor = 4
    exchanges_collection = []
    counter = 0

    length = len(markets) // 4
    page_no = 1

    while exchange_names or market_pairs:
        if counter % 4 == 0:
            market_embed = Embed(title = f"Exchanges (Page {page_no}/{length})", color = 0x74bbfc)
            market_embed.set_footer(text = "Perpetual Protocol", icon_url=icon_url)
            market_embed.set_thumbnail(url=icon_url)

        exchange_link, market_pair = next(market_pairs, (None, None))
        if not exchange_link:
            break

        if not old_name:
            exchange_name = next(exchange_names, None)

        exchange_emote = get_emote(exchange_name)
        old_name = exchange_name

        exchange_name = next(exchange_names, None)
        market_desc = f"{exchange_emote}" + " \u200b " * factor + \
            f"[Trade Here]({exchange_link})" + " \u200b " * factor + \
            f"{market_pair}\n\n"


        # Duplicate exchange names, but different pairs.
        # E.g. PERP/ETH on Binance AND PERP/USDT on Binance.
        while old_name == exchange_name:
            exchange_link, market_pair = next(market_pairs, None)
            market_desc += f"{exchange_emote}" + " \u200b " * factor + \
            f"[Trade Here]({exchange_link})" + " \u200b " * factor + \
            f"{market_pair}\n\n"
            old_name = exchange_name
            exchange_name = next(exchange_names, None)

        market_embed.add_field(name = old_name, value = market_desc, inline = False)
        if counter == 3 or counter > 0 and counter % 4 == 0:
            page_no += 1
            exchanges_collection.append(market_embed)
        counter += 1

    return exchanges_collection
